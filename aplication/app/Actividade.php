<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividade extends Model
{
    protected $fillable = [
        'nome', 'objectivo','execucao','percentagemExecucao','periodo','ano','obs'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function periodos()
   {
      return $this->belongsToMany('App\Periodo', 'actividade_periodo', 'actividade_id', 'periodo_id');
   }

}
