<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagamentoResidencia extends Model
{
    protected $fillable = [
        'valor','mes', 'ano','forma_pago', 'obs'
      ];

      public function Interno()
      {
         return $this->belongsTo('App\Interno');
      }
      public function Inscricao()
      {
         return $this->belongsTo('App\InscricaoResidencia');
      }
      public function EmolumentoResidencia()
      {
         return $this->belongsTo('App\EmolumentoResidencia');
      }
}
