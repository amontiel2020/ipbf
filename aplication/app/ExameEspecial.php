<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExameEspecial extends Model
{
    protected $fillable = ['anoAcademico'];


    public function Estudante()
    {
        return $this->belongsTo('App\Estudante');
    }
    public function Disciplina()
    {
        return $this->belongsTo('App\Disciplina');
    }
}
