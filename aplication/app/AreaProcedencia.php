<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaProcedencia extends Model
{
    protected $fillable = ['nome','descricao'];
    
    public function procedencia()
    {
        return $this->belongsTo('App\Procedencia');
    }
}
