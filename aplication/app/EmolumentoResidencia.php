<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmolumentoResidencia extends Model
{
    protected $fillable = [
        'nome', 'valor', 'descricao'
     ];


}
