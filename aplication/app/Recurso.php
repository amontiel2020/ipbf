<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Recurso extends Model
{
    protected $fillable = ['anoAcademico'];


    public function Estudante()
    {
        return $this->belongsTo('App\Estudante');
    }
    public function Disciplina()
    {
        return $this->belongsTo('App\Disciplina');
    }
}
