<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quarto extends Model
{
    protected $fillable=['codigo','estado','capacidad','preco','descricao'];

    public function Base()
    {
        return $this->belongsTo('App\Base');
    }
}
