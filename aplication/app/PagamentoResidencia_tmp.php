<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagamentoResidencia_tmp extends Model
{
    protected $fillable = [
        'valor','mes', 'ano','formaPago', 'obs'
      ];

      public function EstInterno()
      {
         return $this->belongsTo('App\Interno');
      }
      public function EmolumentoResidencia()
      {
         return $this->belongsTo('App\EmolumentoResidencia');
      }
      public function InscricaoResidencia()
      {
         return $this->belongsTo('App\InscricaoResidencia');
      }
}
