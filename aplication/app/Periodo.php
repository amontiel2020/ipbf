<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periodo extends Model
{
    protected $fillable=['mes','ano'];


    public $timestamps = false;
}
