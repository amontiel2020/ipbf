<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use \App\InscricaoResidencia;

class Interno extends Model
{
    protected $fillable = [
        'nome','estado','dataEntrada', 'dataSaida'
     ];

     public function Estudante()
     {
        return $this->belongsTo('App\Estudante');
     }
     /*public function Quarto()
     {
        return $this->belongsTo('App\Quarto');
     }*/

     public static function inscricaoVencida($interno_id){
      $interno=Interno::find($interno_id);
      $dataActual = Carbon::now();
      
      if($interno->estado==1){
         $inscricao=InscricaoResidencia::where("interno_id",$interno_id)->where("estado",1)->first();
         $dataSaida=$inscricao->data_saida;
      }


     }
     public static function getDataEntrada($interno_id){
     $interno=Interno::find($interno_id);
      return $interno->created_at;
     }
     public static function getDataSaida($interno_id){
      $inscricao=InscricaoResidencia::where("interno_id",$interno_id)->first();
      if($inscricao!=null){
         return $inscricao->data_saida;

      }else{
         return null;
      }
     }
}
