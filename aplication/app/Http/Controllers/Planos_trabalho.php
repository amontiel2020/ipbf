<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade as PDF;

use App\Actividade;
use App\Funcionario;
use App\Departamento;
use App\PlanoTrabalho;

class Planos_trabalho extends Controller
{
   public function index()
   {
      return view("planos_trabalho.index");
   }

   public function saveActividade(Request $request)
   {


      $user = Auth::user();
      $actividade = new Actividade();

      $actividade->nome = $request->nome;
      $actividade->objectivo = $request->objectivo;
      $array_periodo = $request->periodo;

      $splitPeriodos = explode("-", $array_periodo);

      $periodo = $splitPeriodos[1];
      $ano = $splitPeriodos[0];

      $actividade->periodo = $periodo;
      $actividade->ano = $ano;

      $actividade->user_id = $user->id;


      $actividade->save();
      return redirect("listaActividades");

      /* if ($request->ajax()) {
         $resultado = "OK";
         return response()->json($resultado);
      }*/
   }

   public function painel()
   {
      $usuario = Auth::user();
      $planos_trabalho = PlanoTrabalho::all();
      return view("planos_trabalho.painel", compact('planos_trabalho'));
   }
   public function listaActividades(Request $request)
   {
      $user = Auth::user();
      $lista = collect();
      if ($request->filtrar != null) {
         $array_periodo = $request->periodo;
         $splitPeriodos = explode("-", $array_periodo);
         $periodo = $splitPeriodos[1];
         $ano = $splitPeriodos[0];
         $lista = Actividade::where("user_id", $user->id)->where("periodo", $periodo)->where("ano", $ano)->get();
      } else if ($request->filtrar == null) {
         $lista = Actividade::where("user_id", $user->id)->get();
      }
      if(Auth::user()->hasRole('Admin')){
         $lista = Actividade::all();

      }

      return view("planos_trabalho.listaActividades", compact('lista'));
   }
   public function addActividade()
   {
      return view("planos_trabalho.addActividade");
   }
   public function editarActividade($id)
   {
      $act = Actividade::find($id);
      $ano = $act->ano;
      $periodo = $act->periodo;
      //$periodo_int=(int)$periodo;
      if ($periodo > 0 && $periodo < 10) {
         $periodo = '0'.$periodo;
      }
      return view("planos_trabalho.editarActividade", compact('act', 'periodo', 'ano'));
   }
   public function storeActividade(Request $request)
   {
      $actividade = Actividade::find($request->id);

      $array_periodo = $request->periodo;
      $splitPeriodos = explode("-", $array_periodo);
      $periodo = $splitPeriodos[1];
      $ano = $splitPeriodos[0];

      $actividade->nome = $request->nome;
      $actividade->objectivo = $request->objectivo;
      $actividade->periodo = $periodo;
      $actividade->ano = $ano;


      $actividade->save();


      return redirect("listaActividades");
   }

   public function eliminarActividade($id)
   {
      $act = Actividade::find($id);
      $plano_id = $act->plano_trabalho_id;

      $act->delete();
      return redirect("listaActividades/" . $plano_id);
   }

   public function addPlanoTrabalho()
   {
      return view("planos_trabalho.addPlanoTrabalho");
   }
   public function storePlanoTrabalho(Request $request)
   {

      $plano = new PlanoTrabalho();
      $usuario = Auth::user();
      $plano->nome = $request->nome;
      $plano->mes = $request->mes;
      $plano->ano = $request->ano;
      $plano->user_id = $usuario->id;
      $plano->save();


      return redirect("planos_trabalho");
   }
   public function execucaoActividade($id)
   {
      $act = Actividade::find($id);

      return view("planos_trabalho.execucaoActividade", compact('act'));
   }

   public function storeExecucaoActividade(Request $request)
   {

      $act = Actividade::find($request->id);

      $act->nome = $request->nome;
      $act->objectivo = $request->objectivo;
      $act->execucao = $request->execucao;
      $act->percentagemExecucao = $request->percentagemExecucao;
      $act->obs = $request->obs;

      $act->save();
      return redirect("execucaoActividade/" . $act->id);
   }
   public function relatorio(Request $request)
   {


      $user = Auth::user();
      $actividades = collect();
      if ($request->filtrar != null) {
         $array_periodo = $request->periodo;
         $splitPeriodos = explode("-", $array_periodo);
         $periodo = $splitPeriodos[1];
         $ano = $splitPeriodos[0];
         $actividades = Actividade::where("user_id", $user->id)->where("periodo", $periodo)->where("ano", $ano)->get();
      } else if ($request->filtrar == null) {
         $periodo = 0;
         $ano = 0;
         $actividades = Actividade::where("user_id", $user->id)->get();
      }
      $width="width:";
      $porciento="%";
      return view("planos_trabalho.relatorio", compact('actividades', 'periodo', 'ano','width','porciento'));
   }

   public function relatorioPDF($periodo, $ano)
   {
      $user = Auth::user();
      $actividades = collect();

      if ($periodo == null && $ano == null) {

         $actividades = Actividade::where("user_id", $user->id)->get();
      } else if ($periodo != null && $ano != null)
         $actividades = Actividade::where("user_id", $user->id)->where("periodo", $periodo)->where("ano", $ano)->get();

      $pdf = PDF::loadView('planos_trabalho.relatorio_pdf', compact('actividades', 'user', 'periodo', 'ano'));
      // return $pdf->download('facturaCandidatura.pdf');
      // redirect()->route('listarCandidatos',1);
      return $pdf->stream();
   }

   public function filtrarActividadesMes(Request $request)
   {
      $user = Auth::user();

      $array_periodo = $request->periodo;

      $splitPeriodos = explode("-", $array_periodo);

      $periodo = $splitPeriodos[1];
      $ano = $splitPeriodos[0];
      $actividades = Actividade::where("user_id", $user->id)->where("periodo", $periodo)->where("ano", $ano)->get();
   }
}
