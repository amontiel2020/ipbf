<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interno;
use App\Estudante;
use App\InscricaoResidencia;
use App\Quarto;
use Carbon\Carbon;



class EstudantesInternos extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $lista = Interno::all();
        $dataActual = Carbon::now();


        return view("estudantesInternos.index", compact('lista','dataActual'));
    }

    public function inserir(Request $request)
    {
        $listaEstudantes = Estudante::where('estado', 'activo')->pluck('nome', 'id'); //->prepend('selecciona');
        $estudante = null;
        $quartos=null;
        if ($request->estudantes) {
            $estudante = Estudante::find($request->estudantes);
            $quartos=Quarto::where("estado",1)->get();
        }

        return view('estudantesInternos.inserir', compact('listaEstudantes','estudante','quartos'));
    }

    public function registrarInscricaoInterno(Request $request){
        $date = Carbon::now();

        $dia=$date->day;
        $mes= $date->month;
        $ano= $date->year;

        $mesesApagar=$request->meses;
        $somaMeses=$mes+$mesesApagar;

        $novoMes=$somaMeses;
        if($somaMeses>12){
            $novoMes=$somaMeses-12;
            $ano=$ano+1;
        }
        //dd($date);


        $estudante=Estudante::find($request->id);
        $quarto=Quarto::find($request->quarto);

        $interno=new Interno();
        $interno->estudante_id=$estudante->id;
        $interno->nome=$estudante->nome;
        $interno->pathImage=$estudante->pathImage;
        
        $interno->estado=1;
      
        $interno->data_entrada=$date;

        $interno->save();

        $inscricao=new InscricaoResidencia();

        $meses=$request->meses;
        $modalidade=$request->modalidade;
        $valorInscricao=$request->valor;

        $inscricao->meses=$meses;
        
        $inscricao->valor=$valorInscricao;
        $inscricao->actual=1;
        $inscricao->data_entrada=$date;
        $stringDataSaida=$ano."-".$novoMes."-".$dia;
        $dataSaida=Carbon::parse($stringDataSaida);
        $inscricao->data_saida=$dataSaida;
        $inscricao->interno_id=$interno->id;
        $inscricao->quarto_id=$quarto->id;
        $inscricao->anoAcademico="2022/2023";

        $inscricao->save();

        return redirect("estudantesInternos/index");

    }
}
