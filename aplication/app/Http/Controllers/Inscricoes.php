<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

use \App\Inscricao;
use \App\Disciplina;
use \App\Estudante;
use \App\Recurso;
use \App\Avaliacao;
use \App\ExameEspecial;

use \App\CONFIGUARCAO;






class Inscricoes extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function eliminarDisciplina($inscricao_id, $disciplina_id)
    {
        $inscricao = Inscricao::find($inscricao_id);

        $inscricao->disciplinas()->detach($disciplina_id);
        $inscricao->save();
        //return  redirect()->route('mostrarFicha', $inscricao->estudante_id);
        return redirect()->route('listarEstudantes');

    }

    public function registrarDisciplinaInscricao(Request $request)
    {
        $inscricao = Inscricao::find($request->inscricao_id);
        $disciplina = Disciplina::find($request->disciplina_id);

        $inscricao->disciplinas()->attach($disciplina);

        $inscricao->save();

        return redirect()->route('listarEstudantes');
    }

    public function gerirInscricoes($id)
    {
        $estudante = Estudante::where('id', $id)->first();
        $disciplinas = Disciplina::where("curso_id", $estudante->curso_id)->get();

        return view('inscricoes.gerirInscricoes', compact('estudante', 'disciplinas'));
    }

    public function inscricoesRecursos($disciplina_id)
    {
        $disciplina = Disciplina::find($disciplina_id);
      //  $listaEstudantes = Estudante::where('estado', 'activo')->pluck('nome', 'id'); //->prepend('selecciona');
        $inscricoes = Inscricao::where('anoAcademico', \App\CONFIGURACAO::getAnoAcademico())->where("curso_id", $disciplina->curso_id)->where("semestre", $disciplina->semestre)->get();
        $listaEstudantes2 = collect();
        foreach ($inscricoes as $inscricao) {
            $estudante = Estudante::where('id', $inscricao->estudante_id)->first();
            $mediaFinal = \App\Pauta::obterMediaFinal($estudante->id, $disciplina_id,\App\CONFIGURACAO::getAnoAcademico());
            if ($mediaFinal != null && $mediaFinal < 10) {
                $listaEstudantes2->push($estudante);
            }
        }

         
        $recursos = Recurso::where("anoAcademico", "2022")->where("disciplina_id", $disciplina_id)->get();

        return view('inscricoes.inscricoesRecursos', compact('disciplina', 'disciplina_id', 'recursos', 'listaEstudantes2'));
    }

    public function inscricoesExameEspecial($disciplina_id)
    {
        $disciplina = Disciplina::find($disciplina_id);
       // $listaEstudantes = Estudante::where('estado', 'activo')->pluck('nome', 'id'); //->prepend('selecciona');
      //  $inscricoes = Inscricao::where('anoAcademico', \App\CONFIGURACAO::getAnoAcademico())->where("curso_id", $disciplina->curso_id)->where("semestre", $disciplina->semestre)->get();
      //  $listaEstudantes2 = collect();
      /*  foreach ($inscricoes as $inscricao) {
            $estudante = Estudante::where('id', $inscricao->estudante_id)->first();
            $mediaFinal = \App\Pauta::obterMediaFinal($estudante->id, $disciplina_id,\App\CONFIGURACAO::getAnoAcademico());
            if ($mediaFinal != null && $mediaFinal < 10) {
                $listaEstudantes2->push($estudante);
            }
        }*/

          $listaEstudantes2 = Estudante::where('estado', 'activo')->where("curso_id",$disciplina->curso_id)->get();
        $examesEspecial = ExameEspecial::where("anoAcademico", "2022")->where("disciplina_id", $disciplina_id)->get();

        return view('inscricoes.inscricoesExameEspecial', compact('disciplina', 'disciplina_id', 'examesEspecial', 'listaEstudantes2'));
    }



    public function storeInscricoesRecursos(Request $request)
    {



        $recurso = new Recurso();
        $recurso->estudante_id = $request->estudante;
        $recurso->disciplina_id = $request->disciplina_id;
        $recurso->anoAcademico = "2022";
        $recurso->save();

        return redirect()->route('listarEstudantes');
    }

    public function storeInscricoesExameEspecial(Request $request)
    {



        $exame = new ExameEspecial();
        $exame->estudante_id = $request->estudante;
        $exame->disciplina_id = $request->disciplina_id;
        $exame->anoAcademico = "2022";
        $exame->save();

        return redirect()->route('listarEstudantes');
    }



    public function gerarPdfActaExameRecurso($id)
    {
        $estudantes = Recurso::where("disciplina_id", $id)->where("anoAcademico", "2022")->get();
        $i = 1;

        $pdf = PDF::loadView('inscricoes.pdfActaExameRecurso', compact('estudantes', 'i'));

        return $pdf->download('listaRecurso.pdf');
    }

    public function gerarPdfActaExameEspecial($id)
    {
        $estudantes = ExameEspecial::where("disciplina_id", $id)->where("anoAcademico", "2022")->get();
        $i = 1;

        $pdf = PDF::loadView('inscricoes.pdfActaExameEspecial', compact('estudantes', 'i'));

        return $pdf->download('listaExameEspecial.pdf');
    }

    public function pautaRecurso($id)
    {
        $idDisc = $id;
        $estudantes = Recurso::where("disciplina_id", $id)->where("anoAcademico", "2022")->get();
        $i = 1;
        return view('inscricoes.pautaRecurso', compact('estudantes', 'idDisc', 'i'));
    }
    public function pautaExameEspecial($id)
    {
        $idDisc = $id;
        $estudantes = ExameEspecial::where("disciplina_id", $id)->where("anoAcademico", "2022")->get();
        $i = 1;
        return view('inscricoes.pautaExameEspecial', compact('estudantes', 'idDisc', 'i'));
    }

    public function confirmarPautaRecurso($idDisc)
    {

        $avaliacoes = Avaliacao::where("disciplina_id", $idDisc)->where("anoAcad", \App\CONFIGURACAO::getAnoAcademico())->get();
        foreach ($avaliacoes as $aval) {

            $aval->estado = 1;
            $aval->save();
        }
        return redirect()->route('pautaRecurso', $idDisc);
    }
    public function confirmarPautaExameEspecial($idDisc)
    {

        $avaliacoes = Avaliacao::where("disciplina_id", $idDisc)->where("anoAcad", \App\CONFIGURACAO::getAnoAcademico())->get();
        foreach ($avaliacoes as $aval) {

            $aval->estado = 1;
            $aval->save();
        }
        return redirect()->route('pautaExameEspecial', $idDisc);
    }
}
