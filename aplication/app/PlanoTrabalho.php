<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanoTrabalho extends Model
{
    protected $fillable = [
        'nome','mes','ano', 'descricao'
    ];

    public function Responsavel()
    {
        return $this->belongsTo('App\User');
    }


}
