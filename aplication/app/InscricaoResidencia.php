<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InscricaoResidencia extends Model
{
    protected $fillable = [
      'meses','valor','actual','dataEntrada', 'dataSaida'
     ];

     public function EstudanteInterno()
     {
        return $this->belongsTo('App\Interno');
     }
     public function Quarto()
     {
        return $this->belongsTo('App\Quarto');
     }
}
