<?php
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization, Accept,charset,boundary,Content-Length');
header('Access-Control-Allow-Origin: *');

use App\Avaliacao;
use App\Inscricao;
use App\Estudante;
use App\Disciplina;
use App\Conta;
use App\InscricaoResidencia;
use App\Interno;
use App\Quarto;
use App\Actividade;
use App\Curso;
use App\PagamentoResidencia;





use Illuminate\Http\Request;
use Carbon\Carbon;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
/*Route::options('/{path}', function() {
    return response('', 200)
        ->header(
          'Access-Control-Allow-Headers', 
          'Origin, Content-Type, Content-Range, Content-Disposition, Content-Description, X-Auth-Token, X-Requested-With')
        ->header('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS, DELETE')
        ->header('Access-Control-Allow-Origin','*')
        ->header('Access-Control-Allow-Credentials',' true');
  })->where('path', '.*');*/

Route::get('/ejemplo', function () {

    return \App\Estudante::where('estado', 'activo')->get();
});

Route::get('/curso/{id}', function ($id) {

    return \App\Curso::where('id', $id)->first();
});
//PAgamentos do estudante 
Route::get('/pagamentos/{id}', function ($id) {

    return \App\Pagamento::where('estudante_id', $id)->get();
});
//pagamentoApi
Route::get('/pagamentos', function () {

    return \App\PagamentoResidencia::all();
});

//getEstudante
Route::get('/getEstudante/{estudante_id}', function ($estudante_id) {

    return \App\Estudante::where("id", $estudante_id)->first();
});

Route::get('/getEstudanteResidencia/{estudante_id}', function ($estudante_id) {

    return \App\Interno::where("id", $estudante_id)->first();
});



Route::get('/getJsonPagamentosTemp2/{id}', function ($estudante_id) {

    return \App\Pagamento_tmp::where('estudante_id', $estudante_id)->get();
});





/*Route::get('/getJsonPagamentosTemp2/{id}', function ($id) {

    return \App\Pagamento::where("id", $id)->get();
});*/


Route::get('/save_pagamentoTemp', function (Request $request) {

    $pagamento = new \App\Pagamento_tmp();
    $pagamento->designacao = "Propinas";
    $pagamento->estudante_id = $request->estudante_id;
    $pagamento->valor = 25000;
    $pagamento->emolumento_id = 1;
    $pagamento->ano = 2022;
    $pagamento->mes = $request->mes;
    $pagamento->taxa = 0;

    $pagamento->save();
});


Route::get('/gerarComprovativo', function () {
    $item = \App\Pagamento::find(3591);
    $total = $item->valor;
    $totalTaxa = $item->taxa;

    $estudante = \App\Estudante::where('id', $item->estudante_id)->first();
    //$item->cant_recibos++;
    $item->save();
    // $pdf = PDF::loadView('pagamentos.pdfReciboSegundaVia2', compact("item", "estudante", "total", "totalTaxa"))->setPaper('a5-R');
    // return $pdf->download('comprovativo.pdf');
});


Route::get('/getPagamentosSemFazer/{estudante_id}/{anoAcademico}', function ($estudante_id, $anoAcademico) {
    $pagamentos_resultado = collect();

    $pagamentos = \App\Pagamento::where("estudante_id", $estudante_id)
        ->where("ano", $anoAcademico)->get();



    for ($mes = 1; $mes <= 10; $mes++) {
        $pagamento_semFazer = new \App\Pagamento();

        $pagamento_semFazer->estudante_id = (int)$estudante_id;
        $pagamento_semFazer->ano = "2022";
        $pagamento_semFazer->valor = null;
        $pagamento_semFazer->mes = (string)$mes;
        $pagamento = $pagamentos->firstWhere("mes", $mes);
        // $pagamento_semFazer->id=$pagamento->id;
        if ($pagamento != null) {
            $pagamentos_resultado->push($pagamento);
        } else {
            $pagamentos_resultado->push($pagamento_semFazer);
        }
    }
    return $pagamentos_resultado->toJson();
});




Route::get('/outrosPagamentos/{id}/{anoAcademico}', function ($id, $anoAcademico) {
    $resultado = Illuminate\Support\Facades\DB::table('pagamentos')
        ->join('emolumentos', function ($join) {
            $join->on('pagamentos.emolumento_id', '=', 'emolumentos.id');
        })
        ->join('estudantes', function ($join) {
            $join->on('pagamentos.estudante_id', '=', 'estudantes.id');
        })
        ->where('estudantes.id', "=", $id)
        // ->where('pagamentos.ano', "=", $anoAcademico)
        ->where('pagamentos.emolumento_id', "<>", 1)
        ->select('pagamentos.valor', 'pagamentos.obs', 'pagamentos.descrip', 'pagamentos.created_at as data', 'emolumentos.nome as nome')
        ->groupBy('pagamentos.valor', 'pagamentos.obs', 'pagamentos.descrip', 'pagamentos.created_at', 'emolumentos.nome')
        ->get();

    // return Pagamento::where('estudante_id', $id)->where('emolumento_id',"<>", 1)->where('ano', $anoAcademico)->get();
    return $resultado;
});




Route::get('/pagamentos_react/{id}', function ($id) {
    return \App\Pagamento::where('estudante_id', $id)->where('emolumento_id', 1)->get();
});

Route::get('/pagamentos_residencia/{id}/{inscricao}', function ($id, $inscricao) {
    $insricao = \App\InscricaoResidencia::where("id", $inscricao)->where("actual", 1)->first();
    return  $pagamentos = \App\PagamentoResidencia::where('inscricao_residencia_id', $insricao->id)->get();
});

//pagamentoApi
Route::get('/pagamentos/{id}/{inscricao}/{mes}', function ($id, $inscricao,$mes) {
  
  $pagamento = \App\PagamentoResidencia::where('interno_id', $id)->where("inscricao_residencia_id",$inscricao)->where("mes",$mes)->first();
  return $pagamento;
});


Route::get('/getCursos', function () {
    return \App\Curso::all();
});




Route::get('/filtrarEstudantes/{turma_id}', function ($turma_id) {
    return \App\Estudante::where('estado', 'Activo')->where('turma_id', $turma_id)->get();
});




Route::get('/filtrarTurmas/{curso_id}', function ($curso_id) {
    return \App\Turma::where("curso_id", $curso_id)->get();
});




Route::get('/getTurmas', function () {
    return \App\Turma::all();
});

Route::get('/getTurma/{id}', function ($id) {
    return \App\Turma::find($id);
});



Route::get('/estudantes_react', function () {
    // return \App\Estudante::where('estado', 'Activo')->get();
    $resultado = Illuminate\Support\Facades\DB::table('estudantes')
        ->join('cursos', function ($join) {
            $join->on('estudantes.curso_id', '=', 'cursos.id');
        })

        ->where('estudantes.estado', 'Activo')
        ->select(
            'estudantes.id',
            'estudantes.nome',
            'estudantes.BI',
            'estudantes.codigo',
            'estudantes.curso_id',
            'estudantes.pathImage',
            'cursos.nome as curso',
            

        )
        ->groupBy(
            'estudantes.id',
            'estudantes.nome',
            'estudantes.BI',
            'estudantes.codigo',
            'estudantes.curso_id',
            'estudantes.pathImage',
            'cursos.nome',

        )
        ->get();

    return $resultado;
});



Route::get('/estudantes_select', function () {
    $estudantes = Estudante::with('id', 'nome')->get();
    return $estudantes;
});
Route::get('/estudantes_residencia', function () {
    // return \App\Estudante::where('estado', 'Activo')->get();
    $resultado = Illuminate\Support\Facades\DB::table('internos')
        ->where('internos.estado', 1)
        ->select(
            'internos.id',
            'internos.nome',
        )
        ->groupBy(
            'internos.id',
            'internos.nome',

        )
        ->get();

    return $resultado;
});

Route::get('/internos', function () {
    // return \App\Estudante::where('estado', 'Activo')->get();
    $resultado = Illuminate\Support\Facades\DB::table('internos')
        ->where('internos.estado', 1)
        ->select(
            'internos.id',
            'internos.nome',
            'internos.pathImage',
            'internos.estado',
            'internos.curso_id',
          

        )
        ->groupBy(
            'internos.id',
            'internos.nome',
            'internos.pathImage',
            'internos.estado',
            'internos.curso_id',


        )
        ->get();

    return $resultado;
});
Route::get('/internos/{id}',function($id){
    $interno=Interno::find($id);
    return $interno;
});
Route::post('/internos/delete',function(Request $request){
    $id=$request->id;
   
    $interno=Interno::find($id);
    $inscricoes=InscricaoResidencia::where("interno_id",$id)->get();

    foreach ($inscricoes as $inscricao) {
               $pagamentos=PagamentoResidencia::where("inscricao_residencia_id",$inscricao->id)->get();

               foreach ($pagamentos as $pagamento) {
                    $pagamento->delete();
               }
               $inscricao->delete();
       
    }
    $interno->delete();
});

Route::get('/estudanteSelected/{id}',function($id){
    $estudante=Estudante::find($id);
    return $estudante;
});

Route::get('/buscar_estudantes/{buscar}', function ($buscar) {

    //return \App\Estudante::where('estado', 'Activo')->where('nome','LIKE', '%' . $buscar . '%')->get();

    $resultado = Illuminate\Support\Facades\DB::table('estudantes')
        ->join('cursos', function ($join) {
            $join->on('estudantes.curso_id', '=', 'cursos.id');
        })
        ->where('estudantes.estado', 'Activo')
        ->where('estudantes.nome', 'LIKE', '%' . $buscar . '%')

        ->select('estudantes.id', 'estudantes.nome', 'estudantes.BI', 'estudantes.codigo', 'estudantes.pathImage', 'cursos.nome as curso')
        ->groupBy('estudantes.id', 'estudantes.nome', 'estudantes.BI', 'estudantes.codigo', 'estudantes.pathImage', 'cursos.nome')
        ->get();

    return $resultado;
});



Route::get('/updatePagamentoTemp/{id}/{taxa}', function ($id, $taxa) {
    $valor = $id->get("id");
    $pag = \App\Pagamento_tmp::find($valor);
    $pag->taxa = $taxa;

    $pag->save();
});



Route::get('/eliminarPagamentoTemp/{id}', function ($id) {
    $pag = \App\Pagamento_tmp::find($id);
    $pag->delete();
});




Route::get('/save_pagamento/{id}', function ($id) {
    $pagamentosTemp = \App\Pagamento_tmp::where("estudante_id", $id)->get();

    foreach ($pagamentosTemp as $p) {
        $pagamento = new \App\Pagamento();
        $pagamento->estudante_id = $p->estudante_id;
        $pagamento->valor = 25000;
        $pagamento->emolumento_id = 1;
        $pagamento->ano = 2022;
        $pagamento->mes = $p->mes;
        $pagamento->taxa = 0;

        $pagamento->save();
        $p->delete();
    }
});

/*Route::get('/save_pagamentoResidencia/{id}', function ($id) {
    $pagamentosTemp = \App\PagamentoResidencia_tmp::where("estInterno_id", $id)->get();

    foreach ($pagamentosTemp as $p) {
        $pagamento = new \App\PagamentoResidencia();
        $pagamento->estResidencia_id = $p->estInterno_id;
        $pagamento->valor = 25000;
        $pagamento->emolumentoResidencia_id = 1;
        $pagamento->ano = 2022;
        $pagamento->mes = $p->mes;
        $pagamento->taxa = 0;

        $pagamento->save();
        $p->delete();
    }
});*/


/*Route::post('/save_pagamentoModuloNovo', function (Request $request) {
   
    $pagamentoTemp = new App\Pagamento();

    $conta=Conta::where("estudante_id",$request->estudante_id)->first();
  
      $pagamentoTemp->estudante_id = $request->estudante_id;
      $pagamentoTemp->valor = $request->punit;
      $pagamentoTemp->taxa = $request->taxa;
      $pagamentoTemp->obs = $request->obs;
      $pagamentoTemp->emolumento_id = $request->emolumento_id;
      $pagamentoTemp->descrip = $request->descrip;
  
      $pagamentoTemp->conta_id = $conta->id;
  
  
      if ($request->mes != null) {
        $pagamentoTemp->mes = $request->mes;
       
      }
      if ($request->ano != null) {
        $pagamentoTemp->ano = $request->ano;
       
      }
      $pagamentoTemp->save();
   
});*/

Route::get('/getCurso/{curso_id}', function ($curso_id) {
    return \App\Curso::where("id", $curso_id)->get();
});




Route::get('getTurma/{turma_id}', function ($turma_id) {
    return \App\Turma::where("id", $turma_id)->first();
});



Route::get('/getEmolumento/{id}', function ($id) {
    return \App\Emolumento::where("id", $id)->first();
});



Route::get('/getPagamento/{estudante_id}/{mes}', function ($estudante_id, $mes) {
    return \App\Pagamento::where("estudante_id", $estudante_id)->where("mes", $mes)->where("ano", 2022)->first();
});



Route::get('/getPagamentosSemFazer/{estudante_id}', function ($estudante_id, $anoAcademico) {
    $pagamentos_resultado = collect();

    $pagamentos = \App\Pagamento::where("estudante_id", $estudante_id)
        ->where("ano", $anoAcademico)->get();



    for ($mes = 1; $mes <= 10; $mes++) {
        $pagamento_semFazer = new \App\Pagamento();

        $pagamento_semFazer->estudante_id = (int)$estudante_id;
        $pagamento_semFazer->ano = "2022";
        $pagamento_semFazer->valor = null;
        $pagamento_semFazer->mes = (string)$mes;
        $pagamento = $pagamentos->firstWhere("mes", $mes);
        // $pagamento_semFazer->id=$pagamento->id;
        if ($pagamento != null) {
            $pagamentos_resultado->push($pagamento);
        } else {
            $pagamentos_resultado->push($pagamento_semFazer);
        }
    }
    return $pagamentos_resultado->toJson();
});




Route::get('/lista_emolumentos', function () {
    return \App\Emolumento::all();
});

Route::get('/getPagamentos/{anoAcademico}', function ($anoAcademico) {
    return \App\Pagamento::where("emolumento_id", 1)->where("ano", $anoAcademico)->get();
});
Route::post('/registrarLivro', 'Livros@create');
/*Route::post('/registrarLivro', function (Request $request) {
    \App\Livro::create([
        'titulo' => $request->titulo,
        'autor' => $request->autor
    ]);

    $response['message'] = "Guardado exitosamente";
    $response['success'] = true;
    return $response;
});*/


//Route::get('/emolumentos', 'Emolumentos@getEmolumentos')->name('getEmolumentos');
Route::get('/emolumentos', function () {
    return \App\Emolumento::all();
});
Route::get('/emolumentos_residencia', function () {
    return \App\EmolumentoResidencia::all();
});

Route::get('/emolumento/{id}', function ($id) {
    return \App\Emolumento::where("id", $id)->first();
});

Route::get('/emolumentoResidencia/{id}', function ($id) {
    return \App\EmolumentoResidencia::where("id", $id)->first();
});



Route::get('/getInscricoes/{idEstudante}', function ($idEstudante) {

    return \App\Inscricao::where("estudante_id", $idEstudante)->get();
});

Route::get('/getRecursos/{idEstudante}', function ($idEstudante) {
    $disciplinas = collect();
    $avals = \App\Avaliacao::where("estudante_id", $idEstudante)->where("anoAcad", 2022)
        ->where("tipo", "Ex2")->where("valor", ">", 0)->get();

    foreach ($avals as $aval) {
        $disc = Disciplina::find($aval->disciplina_id);
        $disciplinas->push($disc);
    }
    return $disciplinas;
});

Route::get('/getPagamentosRecursos/{idEstudante}', function ($idEstudante) {
    $estudante = \App\Estudante::find($idEstudante);
    $pagamentos = \App\Pagamento::where("estudante_id", $estudante->id)->whereYear('created_at', '=', "2022")->where("emolumento_id", 21)->get();
    return $pagamentos;
});

Route::get('/getDiscInscricao/{idInscricao}', function ($idInscricao) {
    $inscricao = Inscricao::find($idInscricao);
    return $inscricao->disciplinas;
    // return  Illuminate\Support\Facades\DB::table('inscricao_disciplina')->where('inscricao_id', $idInscricao)->get();
});
Route::get('/getDiscParaInscricao/{idInscricao}', function ($idInscricao) {
    $inscricao = Inscricao::find($idInscricao);
    $disciplinas = Disciplina::where("curso_id", $inscricao->curso_id)
        ->where("ano", $inscricao->anoCurricular)
        ->where("semestre", $inscricao->semestre)->get();

    return $disciplinas;
    // return  Illuminate\Support\Facades\DB::table('inscricao_disciplina')->where('inscricao_id', $idInscricao)->get();
});

Route::post('/getDiscParaInscricao2', function (Request $request) {
    $estudante = Estudante::find($request->estudante_id);
    $disciplinas = Disciplina::where("curso_id", $estudante->curso_id)
        ->where("ano", $request->anoCurricular)
        ->where("semestre", $request->semestre)->get();

    return $disciplinas;
    // return  Illuminate\Support\Facades\DB::table('inscricao_disciplina')->where('inscricao_id', $idInscricao)->get();
});

Route::get('/deleteInscricao/{idInscricao}/{estudanteSel}', function ($idInscricao, $estudanteSel) {
    $inscricao = Inscricao::find($idInscricao);
    // $estudante=Estudante::find($estudanteSel);

    $disciplinasInsc = $inscricao->disciplinas;

    foreach ($inscricao->disciplinas as $disciplina) {

        $avals = Avaliacao::where("estudante_id", $estudanteSel)->where("disciplina_id", $disciplina->id)->get();
        foreach ($avals as $aval) {
            $aval->delete();
        }
        $inscricoesDisc = Illuminate\Support\Facades\DB::table('inscricao_disciplina')->where('inscricao_id', $idInscricao)->delete();
        //dd($inscricoesDisc);
        /* foreach ($inscricoesDisc as $inscDisc) {
         $inscDisc->delete();
      }*/
    }
    $inscricao->delete();
    // return  Illuminate\Support\Facades\DB::table('inscricao_disciplina')->where('inscricao_id', $idInscricao)->get();
});

Route::post('/addInscricao', function (Request $request) {
    //  error_log("Entro aqui teste");
    $estudante = Estudante::find($request->estudante_id);
    $disciplinas = json_decode($request->disciplinas);
    //inscriçao
    $inscricao = new Inscricao();
    $inscricao->estudante_id = $estudante->id;
    $inscricao->curso_id = $estudante->curso_id;
    $inscricao->anoCurricular = $request->anoCurricular;
    $inscricao->anoAcademico = $request->anoAcad;
    $inscricao->semestre = $request->sem;
    $inscricao->save();

    //  $disciplinas = is_array($disciplinas) ? $disciplinas : array($disciplinas);
    foreach ($disciplinas as $disciplina_id) {
        $disc = Disciplina::find($disciplina_id);
        $inscricao->disciplinas()->attach($disc);
        $inscricao->save();
    }
});


Route::post('/addDisciplinasInscricao', function (Request $request) {
    // dd($request->inscricao_id);
    $inscricao = Inscricao::find($request->inscricao);
    //dd($request->inscricao);
    $disciplinas = $request->listaDisciplinas;
    $arrayDisc = explode(",", $disciplinas);

    // $disciplinas = Disciplina::where("curso_id", $estudante->curso_id)->where("ano", "1º")->where("semestre", "I")->get();
    $tam = sizeof($arrayDisc);
    for ($i = 0; $i < $tam; $i++) {
        $disc = Disciplina::find($arrayDisc[$i]);
        $inscricao->disciplinas()->attach($disc);
    }
    $inscricao->save();
});

Route::get('/getDividas/{id}', function ($id) {

    $conta = Conta::where("estudante_id", $id)->first();
    $dividas = $conta->totalTaxas;
    return $dividas;
});
Route::get('/setDividas/{id}/{valor}', function ($id, $valor) {

    $conta = Conta::where("estudante_id", $id)->first();
    $dividas = $conta->totalTaxas;
    $novoValor = $dividas + $valor;
    $conta->totalTaxas = $novoValor;
    $conta->save();
    return $conta->totalTaxas;
});

Route::get('/tutorials', function () {
    $estudantes = Estudante::where("estado", "Activo")->get();
    return $estudantes;
});

Route::get('/tutorials/{title}', function ($title) {
    $tutorials = Estudante::where("estado", "Activo")->where("nome", 'like', '%' . $title . '%')->get();
    return $tutorials;
});
Route::get('/tutorials/{id}', function ($id) {
    $estudante = Estudante::where("id", $id)->first();
    dd($estudante);
    return $estudante;
});
Route::put('/tutorials/{id}', function ($id, Request $request) {
    $data = $request->data;
    $estudante = Estudante::find($id);
    $estudante->nome = $data->nome;
    $estudante->apelido = $data->apelido;
    $estudante->estado = $data->estado;

    $estudante->save();
});
Route::post('/tutorials', function (Request $request) {

    $estudante = Estudante::find($request->id);
    $estudante->nome = $request->nome;
    $estudante->apelido = $request->apelido;
    $estudante->estado = $request->estado;

    $estudante->save();
});

Route::get('/getEstudantesTurmas/{id}', function ($id) {
    return \App\Estudante::where("estado", "Activo")->where("turma_id", $id)->get();
});

Route::get('/getInscricoes/{discId}/{anoAcad}', function ($discId, $anoAcad) {
    $inscricoes = Inscricao::where('anoAcademico', $anoAcad)->get();
    $estudantes = collect();

    foreach ($inscricoes as $inscricao) {
        foreach ($inscricao->disciplinas as $disciplina) {
            if ($disciplina->pivot->disciplina_id == $discId) {
                $estudante = Estudante::where('id', $inscricao->estudante_id)->first();
                $estudantes->push($estudante);
            }
        }
    }
    return $estudantes;
});


Route::get('/getDisciplinas', function () {
    return \App\Disciplina::all();
});



Route::get('getAvaliacao/{estudante_id}/{disciplina_id}/{tipo}/{anoAcad}', function ($estudante_id, $disciplina_id, $tipo, $anoAcad) {
    $aval = \App\Avaliacao::where("estudante_id", $estudante_id)->where("disciplina_id", $disciplina_id)->where("tipo", $tipo)->where("anoAcad", $anoAcad)->first();
    if ($aval) {
        return $aval->valor;
    } else {
        return 0;
    }
});


Route::get('/getValorInscricao/{inscricao_id}', function ($inscricao_id) {
    $inscricao = InscricaoResidencia::find($inscricao_id);
    //  $quarto=Quarto::find($inscricao->quarto_id);
    return $inscricao->valor;
});


Route::get('/getInscricoes_residencia/{interno_id}', function ($interno_id) {
    $inscricoes = InscricaoResidencia::where("interno_id", $interno_id)->get();

    return $inscricoes;
});



//inscricaoAPI
Route::get('/inscricoes_residencia', function () {
    $inscricoes = InscricaoResidencia::all();

    return $inscricoes;
});

Route::post('/inscricoes_residencia', function (Request $request) {


   // echo "entroAqui";
   
    $date = Carbon::now();

    $dia = $date->day;
    $mes = $date->month;
    $ano = $date->year;

    $mesesApagar = $request->meses;
    $somaMeses = $mes + $mesesApagar;

    $novoMes = $somaMeses;
    if ($somaMeses > 12) {
        $novoMes = $somaMeses - 12;
        $ano = $ano + 1;
    }

    $interno=new Interno();
    $interno->nome=$request->nome;
    $interno->estado=1;
    $interno->data_entrada=$date;

    $stringDataSaida = $ano . "-" . $novoMes . "-" . $dia;
    $dataSaida = Carbon::parse($stringDataSaida);
    $interno->data_saida = $dataSaida;
    $interno->estudante_id=$request->estudante_id;
    $interno->pathImage=$request->pathImage;
    $interno->curso_id=$request->curso_id;

    $interno->save();
   

    $mesesApagar = $request->meses;
    $somaMeses = $mes + $mesesApagar;


    $inscricao = new InscricaoResidencia();

    $inscricao->meses = $request->meses;
    $inscricao->valor = $request->valor;
    $inscricao->anoAcademico = $request->anoAcademico;
    $inscricao->actual = 1;
    $inscricao->data_entrada = $date;
    $stringDataSaida = $ano . "-" . $novoMes . "-" . $dia;
    $dataSaida = Carbon::parse($stringDataSaida);
    $inscricao->data_saida = $dataSaida;
    $inscricao->interno_id = $interno->id;
    $inscricao->quarto_id = $request->quarto_id;

    $inscricao->save();
});

Route::get('/inscricoes_residencia/{interno_id}', function ($interno_id) {
    $inscricoes = InscricaoResidencia::where("interno_id", $interno_id)->get();

    return $inscricoes;
});

Route::get('/inscricao_residencia/{id}', function ($id) {
    $inscricao = InscricaoResidencia::find($id);

    return $inscricao;
});

Route::get('/getActividades/{funcionario_id}', function ($funcionario_id) {

    $actividades = Actividade::where("funcionario_id", $funcionario_id)->get();
    return $actividades;
});
Route::post('/saveActividade', function (Request $request) {
    $actividade = new Actividade();

    $actividade->nome = $request->nome;
    $actividade->meta = $request->meta;
    $actividade->indicador = $request->indicador;
    $actividade->departamento_id = $request->area;
    $actividade->funcionario_id = $request->responsavel;
    $actividade->save();
});

Route::get('/getFuncionarios', function () {

    $funcionarios = \App\Funcionario::all();

    return $funcionarios;
});


Route::get('/getInternos_residencia', function () {

    $internos = \App\Interno::all();

    return $internos;
});
Route::get('/getQBases_residencia', function () {

    $bases = \App\Base::all();

    return $bases;
});

Route::get('/getQuartos_residencia', function () {

    $quartos = \App\Quarto::all();

    return $quartos;
});
Route::get('/quartos', function () {

    $quartos = \App\Quarto::all();

    return $quartos;
});
Route::get('/quartos/{id}', function ($id) {

    $quartos = \App\Quarto::where("base_id",$id)->get();

    return $quartos;
});

Route::get('/quartos/getInternos/{quarto_id}', function ($quarto_id) {
    $internos=collect();
    $inscricoes = \App\InscricaoResidencia::where("actual", 1)->where("quarto_id", $quarto_id)->get();
   
    foreach ($inscricoes as $inscricao) {
       $interno=Interno::find($inscricao->interno_id);
       $internos->push($interno);
    }
   
    return $internos;
});




Route::get('/getOcupadosQuarto_residencia/{quarto_id}', function ($quarto_id) {

    $qtd = \App\InscricaoResidencia::where("actual", 1)->where("quarto_id", $quarto_id)->count();

    return $qtd;
});
Route::get('/getInternosQuarto_residencia/{quarto_id}', function ($quarto_id) {
    $internos=collect();
    $inscricoes = \App\InscricaoResidencia::where("actual", 1)->where("quarto_id", $quarto_id)->get();
   
    foreach ($inscricoes as $inscricao) {
       $interno=Interno::find($inscricao->interno_id);
       $internos->push($interno);
    }
   
    return $internos;
});

Route::get('/getBase_residencia/{id}', function ($id) {

    $base = \App\Base::find($id);

    return $base;
});

//baseApi

Route::get('/bases', function () {

    $bases = \App\Base::all();

    return $bases;
});
Route::get('/bases/{id}', function ($id) {

    $base = \App\Base::find($id);

    return $base;
});

Route::get('/getQuartosBase_residencia/{id}', function ($id) {

    $quartos = \App\Quarto::where("base_id", $id)->get();

    return $quartos;
});

Route::get('/eliminarInscricao_residencia/{inscricao_id}', function ($inscricao_id) {

    $inscricao = \App\InscricaoResidencia::where("id", $inscricao_id)->first();
    $pagamentos = \App\PagamentoResidencia::where('inscricao_residencia_id', $inscricao->id)->get();

    foreach ($pagamentos as $pagamento) {
        $pagamento->delete();
    }
    $inscricao->delete();
});



Route::get('/cancelarInscricao_residencia/{interno_id}', function ($interno_id) {

    $inscricao = \App\InscricaoResidencia::where("interno_id", $interno_id)->where("actual",1)->first();
   $inscricao->actual=0;
   $inscricao->save();

});
Route::post('/registrarInscricaoInterno', function (Request $request) {

    $date = Carbon::now();

    $dia = $date->day;
    $mes = $date->month;
    $ano = $date->year;

    $mesesApagar = $request->meses;
    $somaMeses = $mes + $mesesApagar;

    $novoMes = $somaMeses;
    if ($somaMeses > 12) {
        $novoMes = $somaMeses - 12;
        $ano = $ano + 1;
    }
    $inscricao = new InscricaoResidencia();

    $inscricao->meses = $request->meses;
    $inscricao->valor = $request->valor;
    $inscricao->anoAcademico = $request->anoAcademico;
    $inscricao->actual = 1;
    $inscricao->data_entrada = $date;
    $stringDataSaida = $ano . "-" . $novoMes . "-" . $dia;
    $dataSaida = Carbon::parse($stringDataSaida);
    $inscricao->data_saida = $dataSaida;
    $inscricao->interno_id = $request->interno_id;
    $inscricao->quarto_id = $request->quarto_id;

    $inscricao->save();
});

Route::post('/registrarInterno', function (Request $request) {

    $date = Carbon::now();

    $interno = new Interno();
    $interno->nome = $request->nome;
    $interno->data_entrada = $date;
    $interno->estado = 1;
    $interno->estudante_id = $request->estudante_id;
    $interno->pathImage = $request->pathImage;

    $interno->save();
});


Route::get('/deleteInterno_residencia/{interno_id}', function ($interno_id) {

    $inscricoes = \App\InscricaoResidencia::where("interno_id", $interno_id)->get();
    
  foreach ($inscricoes as $inscricao) {

    $pagamentos = \App\PagamentoResidencia::where('inscricao_residencia_id', $inscricao->id)->get();

    foreach ($pagamentos as $pagamento) {
        $pagamento->delete();
    }
    $inscricao->delete();
  }

});



Route::post('/save_pagamentoResidencia', function (Request $request) {


  $inscricao=InscricaoResidencia::find( $request->inscricao_residencia_id);
  $valorInscricao=$inscricao->valor;
  $pagamentoTemp = new \App\PagamentoResidencia();

//$conta=Conta::where("estudante_id",$request->estudante_id)->first();

  $pagamentoTemp->interno_id = $request->interno_id;
 // $pagamentoTemp->preco_unit = $request->punit;
 // $pagamentoTemp->qtd = $request->qtd;
  $pagamentoTemp->valor = $valorInscricao;
  //$pagamentoTemp->taxa = $request->taxa;
  //$pagamentoTemp->desconto = $request->desconto;
  $pagamentoTemp->forma_pago = $request->forma_pago;
  $pagamentoTemp->emolumento_residencia_id = $request->emolumento_residencia_id;
  $pagamentoTemp->obs = $request->obs;

  $pagamentoTemp->inscricao_residencia_id = $request->inscricao_residencia_id;


  //$pagamentoTemp->conta_id = $conta->id;


  //if ($request->mes != null) {
    $pagamentoTemp->mes = $request->mes;
   
  //}
  //if ($request->ano != null) {
    $pagamentoTemp->ano = $request->ano;
   
 // }
  /*if($request->divida==1){
   
    $valorDivida=$conta->totalTaxas;
    $novoValor=$valorDivida-$request->valor;
    $conta->totalTaxas=$novoValor;
    $conta->save();

 

  }*/
  $pagamentoTemp->save();
 
  
});

//CursoApi
Route::get('/cursos', function () {
    $cursos=Curso::all();
    return $cursos;
 });

Route::get('/cursos/{id}', function ($id) {
   $curso=Curso::find($id);
   return $curso;
});

