@extends('layouts.Main')

@section('content')

@section('estilos')

@endSection

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">


    <form role="form" action="{{route('storeActividade')}}" id="createEstudante_form" enctype="multipart/form-data" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="{{ $act->id }}">


        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Registrar Actividade</h3>
                </div>
                <div class="panel-body">

                    <div class="row">


                        <!-- Painel Identifiação do colaborador -->
                        <div class="panel panel-default">

                            <div class="panel-heading">

                            </div>
                            <div class="panel-body">

                                <div class="form-group">
                                    <label>Actividade</label>
                                    <textarea type="text" name="nome" class="form-control ckeditor" value="{{$act->nome}}">{{$act->nome}}</textarea>

                                </div>


                                <div class="form-group">
                                    <label>Objectivo</label>
                                    <textarea name="objectivo" type="text" class="form-control ckeditor" value="{{$act->objectivo}}">{{$act->objectivo}}</textarea>

                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Periodo</label>
                                            <input name="periodo" type="month" class="form-control per_date_inputs" value="{{$ano}}-{{$periodo}}">


                                        </div>
                                    </div>
                                    <div class="col-md-9"></div>

                                </div>


                            </div>









                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-12">

                            <div class="col-lg-6">
                                <button id="botonConfirmar" class="btn btn-primary btn-block" type="submit">Salvar</button>
                            </div>
                            <div class="col-lg-6">
                                <button class="btn btn-outline btn-success btn-block">Cancelar</button>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
</div>
</form>







@section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    $(document).ready(function() {

        $("#createEstudante_form").validate({


            rules: {
                nomeCompleto: {
                    required: true,
                    maxlength: 50,
                    //lettersonly: true 
                },

                telefone1: {
                    required: true,
                    minlength: 11,
                },
                telefone2: {
                    required: true,
                    minlength: 11,
                },
                telefoneEmergencia: {
                    required: true,
                    minlength: 11,
                },

                apelido: {
                    required: true,
                    maxlength: 500,
                },
                BI: {
                    required: true,
                    maxlength: 14,
                    minlength: 14,

                },


            },
            messages: {

                nomeCompleto: {
                    required: "É obrigatória a indicação de um valor para o campo nome.",
                },
                apelido: {
                    required: "É obrigatória a indicação de um valor para o campo apelido.",
                },
                telefone1: {
                    required: "É obrigatório.",
                    minlength: "Devem ser 9 digitos",
                },
                telefone2: {
                    required: "É obrigatório.",
                    minlength: "Devem ser 9 digitos",
                },
                telefoneEmergencia: {
                    required: "É obrigatório.",
                    minlength: "Devem ser 9 digitos",
                },
                BI: {
                    required: "É obrigatório.",
                    minlength: "Devem ser 14 digitos",
                },

            },
        })

    });

    $("#provNaturaliade").change(function(event) {
        var id_prov = $(this).val();

        url = "/municipios/" + id_prov + "";

        $.getJSON(url, function(response, state) {
            $('#munNaturalidade').empty();

            $.each(response, function(k, v) {
                $('#munNaturalidade').append('<option value=' + v.id + '>' + v.nome + '</option>');

            });



        });

    });

    $("#provEndereco").change(function(event) {
        var id_prov = $(this).val();

        url = "/municipios/" + id_prov + "";

        $.getJSON(url, function(response, state) {
            $('#munEndereco').empty();

            $.each(response, function(k, v) {
                $('#munEndereco').append('<option value=' + v.id + '>' + v.nome + '</option>');

            });



        });

    });



    $("#telefone1, #telefone2, #telefoneEmergencia").mask("Z00-000-000", {
        translation: {
            Z: {
                pattern: /[9]/
            }
        }
    }).attr('minlength', 9);

    $("#BI").mask("000000000AA000", {
        translation: {
            A: {
                pattern: /['BA']/
            }
        }
    }).attr('minlength', 14);

    $(document).ready(function() {
        $('#open').on('click', function() {
            $('#popup').fadeIn('slow');
            $('.popup-overlay').fadeIn('slow');
            $('.popup-overlay').height($(window).height());
            return false;
        });

        $('#close').on('click', function() {
            $('#popup').fadeOut('slow');
            $('.popup-overlay').fadeOut('slow');
            return false;
        });
    });

    /*	$(function() {
    		$("#telefone1").mask('ZA', {
    			translation: {
    				Z: {
    					pattern: /[9]/
    				},
    				A: {
    					pattern: /[0-9]/,
    					recursive: true
    				}
    			}
    		});
    	});*/

    /*		function ValidaTel(val) {
    			if (val.length < 11)
    				alert("O telefone não pode ter menos de 9 digitos");
    		}*/
    //onBlur="javascript: ValidaTel(this.value);"






    function contar() {
        var checkboxes = document.getElementById("form1").checkbox; //Array que contiene los checkbox

        var cont = 0; //Variable que lleva la cuenta de los checkbox pulsados

        for (var x = 0; x < checkboxes.length; x++) {
            if (checkboxes[x].checked) {
                cont = cont + 1;
            }
        }

        alert("El número de checkbox pulsados es " + cont);
    }
</script>
@endSection


@stop