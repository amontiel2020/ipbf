@extends('layouts.Main')
@section('estilos')
<style>
    body {
        font-size: 11px;
    }





    p {
        /* font-size: 8px !important;*/
        line-height: 1.0 !important;
    }

    p.logo {
        /* font-size: 8px !important;*/
        line-height: 0.5 !important;
    }
</style>
@endSection
@section('content')

<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <h3>Relatorios de Actividades</h3>
            <div class="text-left">
                <a href="#">Trimestral</a> | <a href="#">Anual</a>
            </div>
            <div class="text-right">
                <a href="{{route('addActividade')}}" class="btn btn-primary btn-sm">Registrar Actividade</a>
            </div>
            <form class="form-inline" action="{{route('relatorioActividades')}}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="filtrar" value="filtrar">


                <div class="form-group">
                    <label for="">Periodo</label>
                    <input type="month" name="periodo" id="periodo" class="form-control">

                </div>
                <button class="btn btn-primary" type="submit">Filtrar</button>
            </form>
        </div>
    </div>


    @if($periodo!=null && $ano!=null && (!$actividades->isEmpty()) )

    <a href="{{route('relatorioPDF',[$periodo,$ano])}}"><i class="fa fa-file-pdf-o fa-4x" aria-hidden="true"></i></a><br><br>
    @endif
    @if (!$actividades->isEmpty())
    @if($periodo!=null && $ano!=null )

   <h3>Actividades  {{$periodo}} / {{$ano}}</h3>
    @endif
    <table class="table table-bordered table-striped">
        <tr>
            <th>Nº</th>
            <th>Actividade </th>
            <th>Objectivo</th>
            @if($periodo==null && $ano==null )
            <th>Periodo</th>
            @endif
            <th>Actividades Realizadas</th>
            <th>% Execução</th>
            <th>Observação</th>

        </tr>
        @foreach ($actividades as $i => $item)
        <tr>
            <td>{{ $i + 1 }}</td>
            <td>{!! $item->nome !!} </td>
            <td>{!! $item->objectivo !!} </td>
            @if($periodo==null && $ano==null )
            <td>{{ $item->periodo }}/{{ $item->ano }} </td>
            @endif
            <td>{!! $item->execucao !!}</td>
            <td>
               
                <div class="progress">
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="{{$width}}{{$item->percentagemExecucao}}{{$porciento}}">
                    {{ $item->percentagemExecucao }}% 
                    </div>
                </div>

            </td>
            <td>{{ $item->obs }}</td>

        </tr>
        @endforeach
    </table>
    @else
    <h3>Sem actividades neste periodo</h3>
    @endif
    @stop