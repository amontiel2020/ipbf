@extends('layouts.Main')
@section('estilos')

@endSection
@section('content')

<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <h3>Lista de Actividades</h3>
     
            <div class="text-right">
                      <a href="{{route('addActividade')}}" class="btn btn-primary btn-sm">Registrar Actividade</a>
            </div>
            <form class="form-inline" action="{{route('listaActividades')}}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="filtrar" value="filtrar">


                <div class="form-group">
                    <label for="">Periodo</label>
                    <input type="month" name="periodo" id="periodo" class="form-control">

                </div>
                <button class="btn btn-primary" type="submit">Filtrar</button>
            </form>
        </div>
    </div>
  <a href="{{route('relatorioActividades')}}">Relatório</a>
    <table class="table table-bordered table-striped">
        <tr>
            <th>Nº</th>
            <th>Actividade </th>
            <th>Objectivo</th>
           @if(Auth::user()->hasRole('Admin'))
           <th>Responsavel</th>
           @endif
            <th>Periodo</th>
            <th>Actividades Realizadas</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </tr>
        @foreach ($lista as $i => $item)
        <tr>
            <td>{{ $i + 1 }}</td>
            <td> {!! $item->nome !!} </td>
            <td> {!! $item->objectivo !!} </td>
            @if(Auth::user()->hasRole('Admin'))
                <td>{{ $item->user->name}}</td>
           @endif
            <td>{{ $item->periodo }} / {{ $item->ano }} </td>

            <td width="10">
                <a href="{{route('execucaoActividade',$item->id)}}" role="button" class="btn btn-success btn-sm">
          <span class="glyphicon glyphicon-check"></span> Editar
</a>
            </td>

            <td width="10"><a href="{{route('editarActividade',$item->id)}}" class="btn btn-xs btn-primary">
                    <i class="fa fa-pencil-square"></i>
                </a>
            </td>

            <td width="10">
                <form action="{{route('eliminarActividade',$item->id)}}">
                    <button onclick="return confirm('Eliminar registro?')" class="btn btn-xs btn-danger">
                        <i class="fa fa-trash"></i>
                    </button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    @stop