@extends('layouts.Main')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Planos de Trabalho</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="text-right">
        <a href="{{route('addPlanoTrabalho')}}" class="btn btn-primary btn-sm">Registrar Plano</a>
        </div>
    
    </div>
    <div class="panel-body">

        <div class="row">
            @foreach ($planos_trabalho as $plano)


            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-tasks fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">Mes:{{$plano->mes}}</div>
                                <div class="huge">Ano:{{$plano->ano}}</div>
                              
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left"><a href="{{route('listaActividades',$plano->id)}}">Acividades</a></span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@stop