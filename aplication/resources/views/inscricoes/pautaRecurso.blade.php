@extends('layouts.Main')

@section('content')




<form action="{{route('gerarPdfPauta')}}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="idDisc" value="{{ $idDisc }}">


    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Recurso de {{\App\Disciplina::toString($idDisc)}} no ano acadêmico 2021/ {{\App\CONFIGURACAO::getAnoAcademico()}}</h4>
        </div>
        <div class="panel-body">
            @if(!$estudantes->isEmpty())
            <div class="panel panel-danger">
                <div class="panel-body">
                    <h3 align="right" style="color:red">Atenção professor !!! </h3>
                    <h4 align="right" style="color:red">Depois de terminar o lançamento das notas, deve pressionar o botão Confirmar</h4>
                </div>
            </div>

            <div class="row">
                <div align="right" class="col-12">
                    <h1 style="color:red">↓ &nbsp;&nbsp;&nbsp;</h1>
                   
                  @if((\App\Pauta::pauta_confirmada($idDisc,"Ex2"))!=1)
                    <a href="{{route('confirmarPautaRecurso',$idDisc)}}" class="btn btn-primary">Confirmar </a>
                    &nbsp;
                  @else
                  <a href="#" class="btn btn-primary" >Confirmar </a>
                  &nbsp;
                  @endif
                </div>
            </div>
            <br>
            <table class="table table-bordered table-striped">

                <tr>
                    <th>Nº</th>
                    <th>Nome do Aluno</th>
                    <th>Avaliação</th>
                </tr>


                @foreach($estudantes as $item)
                @if($item != null)

                <tr>
                    <td>{{$i++}}</td>
                    <td>{{\App\Estudante::toString($item->estudante_id)}}</td>
                    <td>

                        @if(((\App\Pauta::obterObjectAvaliacao($item->estudante_id,$idDisc,"Ex2",\App\CONFIGURACAO::getAnoAcademico()))==null) || ((\App\Pauta::obterObjectAvaliacao($item->estudante_id,$idDisc,"Ex2",\App\CONFIGURACAO::getAnoAcademico()))->estado==0))

                        <a href="#" class="editAvalEx2" data-pk="{{$item->estudante_id}}" data-name="{{$idDisc}}">
                            @if(\App\Pauta::obterAvaliacaoSemConfirmar($item->estudante_id,$idDisc,"Ex2",\App\CONFIGURACAO::getAnoAcademico())!="")
                            {{number_format(\App\Pauta::obterAvaliacaoSemConfirmar($item->estudante_id,$idDisc,"Ex2",\App\CONFIGURACAO::getAnoAcademico()),1)}}
                            @endif
                        </a>

                        @endif
                        @if(((\App\Pauta::obterObjectAvaliacao($item->estudante_id,$idDisc,"Ex2",\App\CONFIGURACAO::getAnoAcademico()))!=null))
                        @if(((\App\Pauta::obterObjectAvaliacao($item->estudante_id,$idDisc,"Ex2",\App\CONFIGURACAO::getAnoAcademico()))->estado==1))
                        @if(\App\Pauta::obterAvaliacao($item->estudante_id,$idDisc,"Ex2",\App\CONFIGURACAO::getAnoAcademico())!="")

                        @if(Auth::user()->hasRole('admin'))
                        <a href="#" class="editAvalEx2" data-pk="{{$item->estudante_id}}" data-name="{{$idDisc}}">

                            {{number_format(\App\Pauta::obterAvaliacao($item->estudante_id,$idDisc,"Ex2",\App\CONFIGURACAO::getAnoAcademico()),1)}}

                        </a>
                        @else
                        <span class="glyphicon glyphicon-ok"></span>
                        {{number_format(\App\Pauta::obterAvaliacao($item->estudante_id,$idDisc,"Ex2",\App\CONFIGURACAO::getAnoAcademico()),1)}}
                        @endif
                        @endif
                        @endif
                        @endif



                    </td>

                </tr>
                @endif
                @endforeach

            </table>
            @endif





        </div>
    </div>
</form>






@section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    $(document).ready(function() {
        $.fn.editable.defaults.ajaxOptions = {
            type: "GET"
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            }
        });

        $('.editAvalF1').editable({
            url: '{{url("avalF1/update")}}',
            source: '{{url("avalF1/load")}}',
            type: 'text',
            emptytext: 'Lançar',
            success: function(response, newValue) {
                console.log('Updated', response)
            }

        });

        $('.editAvalF2').editable({
            url: '{{url("avalF2/update")}}',
            source: '{{url("avalF2/load")}}',
            type: 'text',
            emptytext: 'Lançar',
            success: function(response, newValue) {
                console.log('Updated', response)
            }

        });

        /*      $('.editAvalF3').editable({
                  url: '{{url("avalF3/update")}}',
                  source: '{{url("avalF3/load")}}',
                   type: 'text',
                  emptytext: 'Nota',
                  success: function(response, newValue) {
                      console.log('Updated', response)
                  }

              });*/

        $('.editAvalEx1').editable({
            url: '{{url("avalEx1/update")}}',
            source: '{{url("avalEx1/load")}}',
            type: 'text',
            emptytext: 'Lançar',
            success: function(response, newValue) {
                console.log('Updated', response)
            }

        });

        $('.editAvalEx2').editable({
            url: '{{url("avalEx2/update")}}',
            source: '{{url("avalEx2/load")}}',
            type: 'text',
            emptytext: 'Lançar',
            success: function(response, newValue) {
                console.log('Updated', response)
            }

        });

        $('.editAvalEx3').editable({
            url: '{{url("avalEx3/update")}}',
            source: '{{url("avalEx3/load")}}',
            type: 'text',
            emptytext: 'Lançar',
            success: function(response, newValue) {
                console.log('Updated', response)
            }

        });

        $('.editAvalMAC').editable({
            url: '{{url("avalMAC/update")}}',
            source: '{{url("avalMAC/load")}}',
            type: 'text',
            emptytext: 'Lançar',
            success: function(response, newValue) {
                console.log('Updated', response)
            }

        });

        $("#checkTodos").click(function() {
            $('input:checkbox').prop('checked', $(this).prop('checked'));
        });

    });
</script>
@endsection
@stop