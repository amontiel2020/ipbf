@extends('layouts.Main')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3>Selecione o estudante para inscrver no Exame Especial de {{$disciplina->nome}} </h3>
    </div>
    <div class="panel-body">
        <form action="{{route('storeInscricoesExameEspecial')}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="disciplina_id" value="{{ $disciplina_id}}">
        
            <select name="estudante" id="estudante">
                @foreach ($listaEstudantes2 as $estudante)
                @if ($estudante->cantidadMesesPagos(2021)>9)
                <option value="{{$estudante->id}}">{{$estudante->nome}}</option>
                @else
                <option value="{{$estudante->id}}" disabled>{{$estudante->nome}}</option>
                @endif

                @endforeach
            </select>
            <button type="submit" class="btn btn-primary">Enviar</button>

        </form>
        <a href="{{route('gerarPdfActaExameEspecial',$disciplina->id)}}">Gerar pdf Acta de Exame</a>
        <table class="table table-bordered table-striped">
            <tr>
                <th>Nº</th>
                <th>Nome Completo</th>
                <th></th>
            </tr>
            @foreach ($examesEspecial as $exame )
            <tr>
                <td></td>
                <td>{{\App\Estudante::toString($exame->estudante_id)}}</td>
                <td></td>
            </tr>
            @endforeach

        </table>
    </div>
</div>


@section('scripts')
<script>
    $(document).ready(function() {


        $('#estudante').select2({

        });

    });
</script>
@endSection

@stop