@extends('layouts.Main')

@section('content')

<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col-md-6">

            </div>
            <div class="col-md-6" align="right">
                <!--    <a href="{{route('inserirEstudantes')}}" class="btn btn-sm btn-primary"><i class="fa fa-plus-circle"></i> Inserir Estudante</a>-->
            </div>
        </div>
    </div>


    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3>Lista de estudantes Internos</h3>
        </div>

        <div class="panel-body">

            <form action="#">
                <div class="row">
                    <div class=col-md-4>
                        <input type="text" class="form-control" name="buscarpor" value="" placeholder="Nome">
                    </div>
                    <div class=col-md-4>
                        <!-- <input type="text" class="form-control" name="buscarporEstado" value="" placeholder="Estado">-->
                        <select class="form-control" name="buscarporEstado" id="buscarporEstado">
                            <option value="1">Activo</option>
                            <option value="0">Não Activo</option>
                            <option value="0">Inscrição Vencida</option>

                        </select>
                    </div>
                    <div class=col-md-4>

                        <button type="submit" class="btn btn-primary" title="Pesquisar">
                            Pesquisar
                        </button>
                    </div>
                </div>
            </form>

            <br>
            <a href="{{route('estudantesInternosInserir')}}" class="btn btn-primary">
                Registrar Interno
            </a>
            <br><br>
            <table class="table table-bordered table-striped">
                <tr>
                    <td>Nº</td>
                    <th>Nome Completo</th>
                    <th>Curso</th>
                    <th>Estado</th>
                    <th>Data Ingreso</th>
                    <th>Valido até</th>
                    <th></th>
                    <th></th>
                </tr>


                @foreach($lista as $i=> $item)
                <tr @if(\App\Interno::getDataSaida($item->id)<$dataActual)
                    style="background-color: red ;"
                    @endif
                    >
                    <td>{{$i+1}}
                    </td>
                    <td>{{$item->nome}} </td>
                    <td></td>
                    <td>
                        @if($item->estado==1)
                        Activo
                        @else
                        Não Activo
                        @endif
                    </td>
                    <td><?php echo date('d-m-Y', strtotime(\App\Interno::getDataEntrada($item->id))) ?></td>
                    <td><?php echo date('d-m-Y', strtotime(\App\Interno::getDataSaida($item->id))) ?></td>

                    <td width="10"><a href="#" class="btn btn-sm btn-primary">
                            <i class="fa fa-pencil-square"></i>
                        </a>
                    </td>
                    <td width="10">
                        <form action="#">
                            <input type="hidden" name="method" value="DELETE">
                            <button onclick="return confirm('Eliminar registro?')" class="btn btn-sm btn-danger">
                                <i class="fa fa-trash"></i>
                            </button>
                        </form>
                    </td>

                </tr>
                @endforeach
            </table>
        </div>
    </div>



    @stop