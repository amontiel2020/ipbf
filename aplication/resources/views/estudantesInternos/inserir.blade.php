@extends('layouts.Main')

@section('content')

<div class="panel panel-primary">
	<div class="panel-heading">
		<h3>Selecione o estudante para registrar</h3>
	</div>
	<div class="panel-body">
		<form action="{{route('estudantesInternosInserir')}}" method="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			{!! Form::select('estudantes',$listaEstudantes,null,['id'=>'estudantes','style'=>'width: 50%']) !!}
			<button type="submit" class="btn btn-primary">Enviar</button>

		</form>
	</div>
</div>
@if(isset($estudante))
<div class="panel panel-default">
	<div class="panel-heading">
		<h1>Inscrição na Residência</h1>
	</div>
	<div class="panel-body">

		<div class="row">
			<div class="col-md-2">
				<img width="150px" height="150px" src="{{url('/storage/'.$estudante->pathImage) }}" alt="">
				<br>
				<p>Nº Estudante: {{$estudante->codigo}}</p>
			</div>
			<div class="col-md-8">
				<br>
				<br>
				<div style="font-size: large;">
					<p><b>Nome:</b>&nbsp;{{$estudante->nome}}&nbsp;{{$estudante->apelido}}</p>
					<p><b>Curso:</b>&nbsp;{{\App\Curso::toString($estudante->curso_id)}}</p>


				</div>
			</div>
		</div>
		<div class="panel panel-primary">
			<div class="panel-body">
				<form action="{{route('registrarInscricaoInterno')}}" method="POST">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input name="id" type="hidden" value="{{$estudante->id}}">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Quarto</label>
								<select name="quarto" class="form-control">
									@foreach($quartos as $quarto)
									<option value="{{$quarto->id}}">{{$quarto->codigo}}--{{$quarto->capacidad}}--{{$quarto->preco}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Modalidade</label>
								<select name="modaliade" class="form-control">
									<option value="1">1 Pessoa</option>
									<option value="2">2 Pessoa</option>
									<option value="3">3 Pessoa</option>
									<option value="4">4 Pessoa</option>
								</select>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Meses</label>
								<select name="meses" class="form-control">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Valor da Inscrição por mês</label>
								<input name="valor" type="text" class="form-control" />
							</div>
						</div>
					</div>
					<div>
						<button type="submit" class="btn btn-primary btn-block">Inscrever</button>
					</div>
			</div>
			</form>
		</div>
	</div>

</div>
@endif


@section('scripts')
<script>
	$(document).ready(function() {

		$('#estudantes').select2({

		});

	});
</script>


@endsection

@stop