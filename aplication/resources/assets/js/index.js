import React from "react";
import { ReactDOM } from "react-dom/client";
import {AppNew} from "./AppNew";
import { Provider } from "react-redux";
import {store} from "./store";


const root=ReactDOM.createRoot(document.getElementById('exampleteste'));
root.render(
    <React.StrictMode>
        <Provider store={store}>
        <AppNew />
        </Provider>
    </React.StrictMode>
)

