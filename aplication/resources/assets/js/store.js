import { configureStore } from "@reduxjs/toolkit";

import { internoApi } from "./services/internoApi";
import { quartoApi } from "./services/quartoApi";

export const store = configureStore({
  reducer: {
    [internoApi.reducerPath]: internoApi.reducer,
    [quartoApi.reducerPath]: quartoApi.reducer
  }
 /* middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(internoApi.middleware)*/
    
   
});
