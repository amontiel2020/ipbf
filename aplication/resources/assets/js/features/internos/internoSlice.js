import { createSlice } from "@reduxjs/toolkit";

export const internoSlice = createSlice({
  name: "internos",
  initialState,
  reducers: {
    addInterno: (state, action) => {
      state.push(action.payload);
    }
  }
});
export const { addInterno } = internoSlice.actions;
export default internoSlice.reducer;
