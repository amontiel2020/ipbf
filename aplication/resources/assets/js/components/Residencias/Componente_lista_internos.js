import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import Modal_Registrar_interno from "./Modal_Registrar_interno";

const Componente_lista_internos = () => {
  const [internos, setInternos] = useState([]);
  const [estudantes, setEstudantes] = useState([]);

  const [show, setShow] = useState(false);

  const getnternos = () => {
    axios
      .get(`http://localhost:8000/api/getInternos_residencia`)
      .then((res) => {
        const internos = res.data;
        setInternos(internos);
      });
  };
  const getEstudantes=()=> {
    axios.get(`http://localhost:8000/api/estudantes_react`).then((res) => {
      const estudantes = res.data;
      setEstudantes(estudantes);

    });
  }
  const deleteInterno_residencia=(interno_id)=> {
    axios.get(`http://localhost:8000/api/deleteInterno_residencia/${interno_id}`).then((res) => {
      

    });
  }
  

  useEffect(() => {
    getnternos();
    getEstudantes()
    /*  return () => {
      second
    }*/
  }, []);

  const deleteInterno=(id)=>{
    deleteInterno_residencia(id);
    getnternos();
  }



  const showModal = () => {
    setShow(true);
  };
  const hideModal = () => {
    setShow(false);
  };

  return (
    <div className="card">
      <div className="card-body">
        <h3 className="card-title">Estudantes Internos</h3>

        <div className="card">
          <div className="card-body">
            <button className="btn btn-primary" onClick={showModal}>Registrar</button>
          </div>
        </div><br/>
        <table className="table table-bordered">
          <thead>
            <tr>
              <td>Nº</td>
              <th>Fotografia</th>
              <th>Nome Completo</th>
              <th>Data Entrada</th>
              <th>Data Saida</th>
              <th></th>
              <th></th>
            </tr>
          </thead>

          {internos.map((interno, i) => (
            <tbody key={interno.id}>
              <tr>
                <td>{i + 1}</td>
                <td>
                  <img
                    className="rounded-circle alineadoTextoImagenCentro"
                    src={"http://localhost:8000/storage/" + interno.pathImage}
                  />
                </td>
                <td>{interno.nome}</td>
                <td>{interno.data_entrada}</td>
                <td>{interno.data_saida}</td>
                <td><a><i className="fa fa-edit fa-3x"></i></a></td>
                <td><a onClick={deleteInterno(interno.id)}><i className="fa fa-trash fa-3x"></i></a></td>

              </tr>
            </tbody>
          ))}
        </table>
      </div>
      <Modal_Registrar_interno estudantes={estudantes} show={show} handleClose={hideModal} getnternos={getnternos} />
    </div>
  );
};

if (document.getElementById("lista_internos")) {
  ReactDOM.render(
    <Componente_lista_internos />,
    document.getElementById("lista_internos")
  );
}
