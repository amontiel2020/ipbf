import React, { useEffect, useState } from "react";
import axios from "axios";
import Componente_quartoT0 from "./Componente_quartoT0";


const Componente_base4 = ({ id }) => {
  const [quartos, setQuartos] = useState([]);
  const [base, setBase] = useState(null);

  const getQuartos = (base_id) => {
    axios
      .get(`http://localhost:8000/api/getQuartosBase_residencia/${base_id}`)
      .then((res) => {
        const quartos = res.data;
        setQuartos(quartos);
      });
  };
  const getBase = (base_id) => {
    axios
      .get(`http://localhost:8000/api/getBase_residencia/${base_id}`)
      .then((res) => {
        const baseResult = res.data;
        setBase(baseResult);
      });
  };

  useEffect(() => {
    getQuartos(id);
    getBase(id);
  }, [id]);
  return (
    <div className="base">
      <div>
        <h3 className="text-center">Base {id}</h3>
      </div>
      <div className="row">
        <div className="col-2"></div>
        <Componente_quartoT0 codigo="09T0" />
        <Componente_quartoT0 codigo="10T0" />
        <div className="col-2 "></div>
      </div>
      <div className="row">
        <div className="col-2 "></div>
        <Componente_quartoT0 codigo="11T0" />
        <Componente_quartoT0 codigo="12T0" />
        <div className="col-2 "></div>
      </div>
      {/* {quartos.map((quarto) => (
        <div className="row">
          <div className="col-2"></div>
          <div className=" col-8 quarto-T0" key={quarto.id}>
            {quarto.codigo}
          </div>
          <div className="col-2"></div>
        </div>
      ))} */}
    </div>
  );
};

export default Componente_base4;
