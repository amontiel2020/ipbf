import React, { useEffect, useState } from "react";
import Componente_pagamentoSemFazer from "./Componente_pagamentoSemFazer";

const Residencia_pagamentosSemFazer = ({
  pagamentosFeitos,
  pagamentoSeleccionado,
  activarPagamento,
  resetActivarPagamento,
  inscricaoSelected
}) => {
  const [activo, setActivo] = useState(true);
  const [pagamentosSemFazer, setPagamentosSemFazer] = useState([]);

  useEffect(() => {
    if (pagamentosFeitos.length == inscricaoSelected.meses) {
      setActivo(false);
    } else {
      obterPagamentosSemFazer();

      setActivo(true);
    }
  }, [pagamentosFeitos]);

  const obterPagamentosSemFazer = () => {
      //pagamentos sem fazer
    var pagamentosTemp = [];
    [...Array(inscricaoSelected.meses)].map((x, i) => {
      if (buscarPagamento(i + 1) == null) {
        var pagamentoSemFazer = {
          key:i+1,
          id: i + 1,
          mes: i + 1,
          inscricao_residencia_id:inscricaoSelected.id,
          ano: inscricaoSelected.anoAcademico,
          interno_id:inscricaoSelected.interno_id,
          emolumento_residencia_id:0,
          punit:inscricaoSelected.valor,
          valor:inscricaoSelected.valor,
          activo: true
        };
        pagamentosTemp.push(pagamentoSemFazer);
      }
    });
    setPagamentosSemFazer(pagamentosTemp);
  
  };

  const buscarPagamento = (mes) => {
    const result = pagamentosFeitos.find((pagamento) => pagamento.mes == mes);
   // console.log("resultado " + result);
    if (result != null) {
      return result.valor;
    } else {
      return null;
    }
  };

  return (
    <div>
      {activo && (
        <div className="card">
          <div className="card-header">
            <p>Pagamentos por Fazer</p>
          </div>
          <div className="card-body">
            <table className="table table-bordered table-striped table-pagamentos">
              <thead>
                <tr>
                  <th>Mes</th>
                  <th>Pagamento</th>
                </tr>
              </thead>
              {pagamentosSemFazer.map((pagamento) => (
                <tbody   key={pagamento.id}>
                  <Componente_pagamentoSemFazer
                    key={pagamento.id}
                    pagamento={pagamento}
                    pagamentoSeleccionado={pagamentoSeleccionado}
                    activarPagamento={activarPagamento}
                    resetActivarPagamento={resetActivarPagamento}
                  />
                </tbody>
              ))}
            </table>
          </div>
        </div>
      )}
    </div>
  );
};

export default Residencia_pagamentosSemFazer;
