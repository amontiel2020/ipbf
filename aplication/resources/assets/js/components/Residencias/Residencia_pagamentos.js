import React from "react";
import Componente_pagamento from "./Componente_pagamento";

const Residencia_pagamentos = ({pagamentos }) => {
  
  return (
    <div className="card">
      <div className="card-header">
        <p>Pagamentos Feitos</p>
      </div>
      <div className="card-body">
        <table className="table table-bordered table-striped table-pagamentos">
          <tbody>
            <tr>
              <th>Mes</th>
              <th>Pagamento</th>
            </tr>
            {pagamentos.map((pagamento) => (
              <Componente_pagamento key={pagamento.id} pagamento={pagamento} />
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Residencia_pagamentos;
