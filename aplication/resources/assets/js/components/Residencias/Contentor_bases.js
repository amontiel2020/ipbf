import React, { useState, useEffect } from "react";
import axios from "axios";
import Componente_base from "./Componente_base";
import Componente_base6 from "./Componente_base6";
import Componente_base4 from "./Componente_base4";
import Inscricao_Modal2 from "./Inscricao_Modal2";




const Contentor_bases = ({interno,handleClosePai}) => {
  const [bases, setBases] = useState([]);
  const [show, setShow] = useState(false);
  const [anoAcademico, setAnoAcademico] = useState(0);
  const [meses, setMeses] = useState(0);
  const [valor, setValor] = useState(0);
  const [quarto, setQuarto] = useState(0);





  const saveInscricao = () => {
    const bodyFormData = new FormData();
    bodyFormData.append("interno_id", interno.id);
    bodyFormData.append("quarto_id", quarto);
    bodyFormData.append("meses", meses);
    bodyFormData.append("valor", valor);
    bodyFormData.append("anoAcademico", anoAcademico);

    axios
      .post("http://localhost:8000/api/registrarInscricaoInterno", bodyFormData)
      .then((res) => {});
  };
  /*useEffect(() => {
    getBases();
  }, []);*/

  /*const getBases = () => {
    axios.get(`http://localhost:8000/api/getBases_residencia`).then((res) => {
      const bases = res.data;
      setQuartos(bases);
    });
  };*/



  const onChangeAnoAcademico=(e)=>{
    const  ano=e.target.value;
      setAnoAcademico(ano);
  }
  
  const onChangeMeses=(e)=>{
   const valor=e.target.value;
    setMeses(valor);
}
const onChangeValor=(e)=>{
  const result=e.target.value;
  setValor(result);
}


 const showModal=()=> {
   setShow(true);
  }
  const hideModal=() =>{
   setShow(false)
  }
  const handleSalvar = () => {
    saveInscricao();
    hideModal();
    handleClosePai();
    setQuarto(0)
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-6">
          <Componente_base showModal={showModal}  setQuarto={setQuarto} id={1} />
        </div>
        <div className="col-6">
        <Componente_base showModal={showModal}  setQuarto={setQuarto} id={2} />
        </div>
      </div>
      <br />
      <br />
      <div className="row">
        <div className="col-6">
        <Componente_base showModal={showModal}  setQuarto={setQuarto} id={3} />
        </div>
        <div className="col-6">
        <Componente_base showModal={showModal}  setQuarto={setQuarto} id={4} />
        </div>
      </div>
      <br />
      <br />
      <div className="row">
        <div className="col-6">
        <Componente_base showModal={showModal}  setQuarto={setQuarto} id={5} />
        </div>
        <div className="col-6">
        <Componente_base showModal={showModal}  setQuarto={setQuarto} id={6} />
        </div>
      </div>
      <Inscricao_Modal2
          show={show}
          handleClose={hideModal}
          onChangeAnoAcademico={onChangeAnoAcademico}
          onChangeMeses={onChangeMeses}
          onChangeValor={onChangeValor}
          handleSalvar={handleSalvar}
         
  
        />
     </div>
  );
};

export default Contentor_bases;

