import React, { Component, useState } from "react";

//const [id,task,completed]=useState({todo});
export default class Residencia_GetMes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nome: ""
    };
    this.getNome = this.getNome.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps != this.props) {
      this.getNome(newProps.mes);
    }
  }
  componentDidMount() {
    this.getNome(this.props.mes);
  }

  getNome(mes) {
    switch (mes) {
      case "1":
        this.setState({ nome: "Mês 1" });
        break;
      case "2":
        this.setState({ nome: "Mês 2" });
        break;
      case "3":
        this.setState({ nome: "Mês 3" });
        break;
      case "4":
        this.setState({ nome: "Mês 4" });
        break;
      case "5":
        this.setState({ nome: "Mês 5" });
        break;
      case "6":
        this.setState({ nome: "Mês 6" });
        break;
      case "7":
        this.setState({ nome: "Mês 7" });
        break;
      case "8":
        this.setState({ nome: "Mês 8" });
        break;
      case "9":
        this.setState({ nome: "Mês 9" });
        break;
      case "10":
        this.setState({ nome: "Mês 10" });
        break;
      case "11":
        this.setState({ nome: "Mês 11" });
        break;
      case "12":
        this.setState({ nome: "Mês 12" });
        break;

      default:
        return nome;
    }
  }

  render() {
    return <span>{this.state.nome}</span>;
  }
}
