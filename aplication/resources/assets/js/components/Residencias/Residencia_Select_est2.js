import React, { useState, useEffect } from "react";
import Select from "react-select";
import axios from "axios";
import { useInternosQuery } from "../../services/internoApi";

const Residencia_Select_est2 = ({getEstudante}) => {
  // this.state = { estudantes: [], options: [], id: "", nome: "" };
  // const[estudantes,setEstudantes]=useState([]);
  const [options, setOptions] = useState([]);
  const [id, setId] = useState("");
  const [nome, setNome] = useState("");

  const { data, error, isLoading, isSuccess } = useInternosQuery();

  // this.getEstudante = this.getEstudante.bind(this);
  // this.handleChange = this.handleChange.bind(this);

  // get all estudantes from backend
  /*const getEstudantes=()=> {
    axios.get(`http://localhost:8000/api/estudantes_residencia`).then((res) => {
      const estudantes = res.data;
    

      const options = estudantes.map((estudante) => ({
        value: estudante.id,
        label: estudante.nome
      }));

      //this.setState({ estudantes: estudantes });
      setEstudantes(estudantes);
     // this.setState({ options: options });
     setOptions(options);
    });
  }
  const getEstudante=(e)=> {
    const { getEstudante } = this.props;
    getEstudante(e.target.value);
  }*/

  /* componentWillMount() {
    this.getEstudantes();
  }*/

  const handleChange = (e) => {
  //  this.setState({ id: e.value, name: e.label });
    setId(e.value);
   // const { getEstudante } = this.props;
   const valor=e.target.value;
    getEstudante(valor);
  };

  return (
    <div className="card">
      <div className="card-body">
        <div className="isErrorIsLoading">
          {error && <p>An error occured</p>}
          {isLoading && <p>Loading...</p>}
        </div>
        {isSuccess && (
          <select name="test" className="form-control" onChange={handleChange}>
            {data.map((estudante) => (
              <option key={estudante.id} value={estudante.id}>{estudante.nome}</option>
            ))}
          </select>
        )}
      </div>
    </div>
  );
};

export default Residencia_Select_est2;
