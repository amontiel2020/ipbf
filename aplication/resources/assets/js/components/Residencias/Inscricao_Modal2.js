import React from "react";
import "../modal.css";
import Contentor_bases from "./Contentor_bases";

const Inscricao_Modal2 = ({ handleClose, show,onChangeAnoAcademico,onChangeMeses,onChangeValor,handleSalvar }) => {
    const showHideClassName = show ? "modal display-block" : "modal display-none";
  return (
    <div className={showHideClassName}>
      <section className="modal-main">
        <div className="card bg-light">
          <div className="card-body">
         
      
            <div className="row">
          
            <div className="form-group">
                <label>Ano Academico</label>
                <select name="anoAcademico" className="form-control" onChange={onChangeAnoAcademico}>
                  <option value="2022/2023">2022/2023</option>
                  <option value="2023/2024">2023/2024</option>
                </select>
              </div>
              <div className="form-group">
                <label>Meses</label>
                <select name="meses" className="form-control" onChange={onChangeMeses}>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </select>
              </div>
              <div className="form-group">
                <label>Valor</label>
                <input type="text" className="form-control" name="valor" onChange={onChangeValor} />
              </div>
             
            </div>
           
          </div>
        </div>
        <button
          className="btn btn-primary btn-sm"
          type="button"
         onClick={handleSalvar}
        >
          Salvar
        </button>
        <button
          className="btn btn-primary btn-sm"
          type="button"
          onClick={handleClose}
        >
          Fechar
        </button>
      </section>
    </div>
  );
};

export default Inscricao_Modal2;
