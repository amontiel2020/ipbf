import React, { useEffect, useState } from "react";

const ItemResidencia_inscricoes = ({
  inscricao,
  indexSelected,
  setInscricaoSelected,
  eliminarInscricao
}) => {
  const [fondo, setFondo] = useState("");
  const [fontSize, setFontSize] = useState(11);
 // const [color, setColor] = useState("");

  const [selected, setSelected] = useState(false);

  const onMouseOver = () => {
    if (!selected) {
      setFondo("red");
    }
  };
  const onMouseOut = () => {
    if (!selected) {
      setFondo("white");
    }
  };
  const handleClick = () => {
    setInscricaoSelected(inscricao);
  //  handleonClickInscricao(inscricao);
    //setInscricaoSelected(inscricao.id);
   
  };

  useEffect(() => {
    if (indexSelected == inscricao.id) {
      setSelected(true);
      setFondo("green");
     // setColor("white");
      setFontSize(16);
    } else if (indexSelected != inscricao.id) {
      setSelected(false);
      setFondo("white");
      //  setColor("black");
      setFontSize(12);
    }
   // console.log(color);
  }, [indexSelected]);

  return (
    <tr
      style={{
        backgroundColor: fondo,
        cursor: "pointer",
        fontSize: fontSize,
       // color: color
      }}
      onMouseOver={onMouseOver}
      onMouseOut={onMouseOut}
      onClick={handleClick}
      key={inscricao.id}
    >
      <td>{inscricao.anoAcademico}</td>
      <td>{inscricao.quarto_id}</td>
      <td>{inscricao.meses}</td>
      <td>{inscricao.valor}</td>
      <td>{inscricao.data_entrada}</td>
      <td>{inscricao.data_saida}</td>
      <td><a onClick={()=>eliminarInscricao(inscricao.id)} ><i className="fa fa-trash"></i>Eliminar</a></td>
    </tr>
  );
};

export default ItemResidencia_inscricoes;
