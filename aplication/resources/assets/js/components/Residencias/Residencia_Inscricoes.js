import React, { Component } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import ItemResidencia_inscricoes from "./ItemResidencia_inscricoes";

export default class Residencia_Inscricoes extends Component {
  constructor(props) {
    super(props);
    this.state = { inscricoes: [] };

    this.getInscricoes = this.getInscricoes.bind(this);
  }

  getInscricoes() {
    const interno_id = this.props.interno.id;
    axios
      .get(`http://localhost:8000/api/getInscricoes_residencia/${interno_id}`)
      .then((res) => {
        const inscricoes = res.data;
        this.setState({ inscricoes: inscricoes });
      });
  }

  componentDidUpdate(newProps) {
    if (newProps != this.props) {
      this.getInscricoes();
    }
  }

  render() {
    if (this.state.inscricoes.length != 0) {
      return (
        <table className="table table-bordered table-striped">
          <tbody>
            <tr>
              <th>Interno</th>
              <th>Quarto</th>
              <th>Meses</th>
              <th>Valor</th>
            </tr>
            {this.state.inscricoes.map((inscricao) => (
            <ItemResidencia_inscricoes key={inscricao.id}  inscricao={inscricao}/>
            ))}
          </tbody>
        </table>
      );
    } else {
      return null;
    }
  }
}
