import React, { useState, useEffect } from "react";
import axios from "axios";

const Comp_internosDisp = ({ capacidade, internos }) => {
  const [disponivel, setDisponivel] = useState(0);

  const calcular = () => {
    const ocupados = internos.length;
    const disponiveis = capacidade - ocupados;
    setDisponivel(disponiveis);
  };

  useEffect(() => {
    calcular();
  }, [internos,capacidade]);

  return (
    <div>
      {[disponivel].map(() => (
        <div className="col-2">
            X
        </div>
      ))}
    </div>
  );
};

export default Comp_internosDisp;
