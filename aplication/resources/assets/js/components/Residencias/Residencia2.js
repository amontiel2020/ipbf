import React, { useState,useEffect} from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import {store} from "../../store";
import axios from "axios";
import Select_est from "./Residencia_Select_est2";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";

import "../estilos.css";
import "bootstrap/dist/css/bootstrap.min.css";

import { BsPlusCircle } from "react-icons/bs";
import { BsDashCircle } from "react-icons/bs";
import Factura from "./Residencia_PDF";
import TableItens from "./Residencia_TableItens";
import PagamentosConta from "./Residencia_PagamentosConta";
import Residencia_Select_emol from "./Residencia_Select_emol";
import Residencia_TableItens from "./Residencia_TableItens";
import Residecia_ModalDelete from "./Residencia_ModalDelete";
import Residencia_Modal from "./Residencia_Modal";
import Residencia_ModalConfirmFactura from "./Residencia_ModalConfirmFactura";
import Residencia_Inscricoes from "./Residencia_Inscricoes";
import Side from "./Side";
import { useInternosQuery } from "../../services/internoApi";



const Residencia2 = () => {
/*  this.state = {
   // contador: 1,
    //contadorItens: 1,
   // itens: [],
   // itemSel: null,
   // pagamentos: [],
   // estudanteSel: null,
   // emolumento: null,
   // show: false,
    //showDelete: false,
    //taxaTemp: 0,
   // indexDelete: null,
    //imprimirFactura: false,
   // showConfirmFactura: false,
    //total: 0,
   // dividas: 0,
  //  valor: 0,
    //activarPagamento: 0,
   // inscricaoSelected: 0
  };*/

  //estados
  const [contador,setContador]=useState(1);
  const [contadorItens,setContadorItens]=useState(1);
  const [itens,setItens]=useState([]);
  const [itemSel,setItemSel]=useState(null);
  const [pagamentos,setPagamentos]=useState([]);
  const [estudanteSel,setEstudanteSel]=useState(null);
  const [emolumento,setEmolumento]=useState(null);
  const [show,setShow]=useState(false);
  const [showDelete,setShowDelete]=useState(false);
  const [taxaTemp,setTaxaTemp]=useState(0);
  const [indexDelete,setIndexDelete]=useState(null);
  const [imprimirFactura,setImprimirFactura]=useState(false);
  const [showConfirmFactura,setShowConfirmFactura]=useState(false);
  const [total,setTotal]=useState(0);
  const [dividas,setDividas]=useState();
  const [valor,setValor]=useState(0);
  const [activarPagamento,setActivarPagamento]=useState(0);
  const [inscricaoSelected,setInscricaoSelected]=useState(0);




const { data:internos, error, isLoading, isSuccess } = useInternosQuery();






const decrementar=()=> {
   setContador(contador-1);
}
const aumentar=()=> {

  setContador(contador+1)
}
const seleccionado=(id)=> {
  // alert(id);
  axios.get(`http://192.168.10.150/api/emolumento/${id}`).then((res) => {
    const emolumento = res.data;
    //console.log(emolumento);
   // this.setState({ emolumento: emolumento });
    setEmolumento(emolumento);
    const itensCopy = Array.from(itens);
    itensCopy.push({
      id: emolumento.id,
      nome: emolumento.nome,
      punit: emolumento.valor,
      qtd: contador,
      taxa: 0,
      desc: 0,
      valor: emolumento.valor * contador,
      ano: 0,
      emolumento_id: emolumento.id,
      obs: "TPA",
      mes: 0,
      descrip: "",
      divida: 0
    });
   // this.setItens(itensCopy);
    setItens(itensCopy);
  });
}
const pagamentoSeleccionado=(pagamento) =>{
  /*  this.setState({
    contadorItens: this.state.contadorItens + 1
  });
  this.setState({
    inscricaoSelected: index
  });*/

  const itensCopy = Array.from(itens);
  //this.getValor();
  itensCopy.push({
    //id: this.state.contadorItens,
    id: pagamento.id,
    nome: "Mês " + pagamento.mes,
    ano: pagamento.ano,
    punit: pagamento.valor,
    qtd: 1,
    valor: pagamento.valor,
    emolumento_residencia_id: pagamento.emolumento_residencia_id,
    inscricao_residencia_id: pagamento.inscricao_residencia_id,
    mes: pagamento.mes,
    forma_pago: "TPA",
    obs: ""
  });
//  this.setItens(itensCopy);
  setItens(itensCopy);
}
const pagamentoDividas=(valor)=> {
  const valorPagar = parseInt(valor);

  const itensCopy = Array.from(itens);
  itensCopy.push({
    id: "Divida",
    nome: "Divida",
    ano: 0,
    punit: valorPagar,
    qtd: 1,
    taxa: valorPagar,
    desc: 0,
    valor: valorPagar,
    emolumento_id: 1,
    mes: 0,
    obs: "TPA",
    descrip: "Multas em atraso",
    divida: 1
  });
//  this.setItens(itensCopy);
  setItens(itensCopy);

}
/*setItens(itens) {
  this.setState({ itens: itens });

}*/

const deleteItem=(index)=> {
  showModalDelete();
 // this.setState({ indexDelete: index });
  setIndexDelete(index);
}

const confirmDelete=()=> {
  var i = itens.findIndex((x) => x.id == indexDelete);
  var item = itens.find((x) => x.id == indexDelete);
  var mes = item.mes;
  var listaItens = [...itens];
  listaItens.splice(i, 1);
  setItens(listaItens);
  
  //this.setState({ indexDelete: null });
  setIndexDelete(null);
  hideModalDelete();
 // this.setState({ activarPagamento: mes });
  setActivarPagamento(mes);
}

const inserirEmolumento=() =>{
  alert("OK");
}

const getEmolumento=(id)=> {
  axios
    .get(`http://192.168.10.150/api/emolumentoResidencia/${id}`)
    .then((res) => {
      const emolumento = res.data;
      //console.log(emolumento);
      //this.setState({ emolumento: emolumento });
      setEmolumento(emolumento);
    });
}
const findInterno=(id)=>{
const interno=internos.find((interno)=>interno.id==id);
return interno;

}

const getEstudante=(selection)=> {
const interno= findInterno(selection);
//this.setState({ estudanteSel: interno });
setEstudanteSel(interno);

 /* axios
    .get(`http://localhost:8000/api/getEstudanteResidencia/${selection}`)
    .then((res) => {
      const estudante = res.data;
      //console.log(estudante);
      this.setState({ estudanteSel: estudante });
      //  this.getPagamentos(this.state.estudanteSel.id);

      //this.getDividas(this.state.estudanteSel.id);
    });*/
}

const getValor=() =>{
  const inscricao_id = inscricaoSelected;
  axios
    .get(`http://localhost:8000/api/getValorInscricao/${inscricao_id}`)
    .then((res) => {
      console.log(res.data);
      const valor = res.data;
      //this.setState({ valor: valor });
      setValor(valor);
    });
}

const getPagamentos=(selection)=>{
  //console.log("Paramentros",selection,anoAcademico);
  axios
    .get(`http://localhost:8000/api/pagamentos_residencia/${selection}`)
    .then((res) => {
      const pagamentos = res.data;
   //   this.setState({ pagamentos: pagamentos });
      setPagamentos(pagamentos);
      //console.log(pagamentos);
    });
}
const savePagamentos=()=> {
  /* const headers = { 
    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
};*/
  const bodyFormData = new FormData();

  bodyFormData.append("interno_id", estudanteSel.id);
  //bodyFormData.append("itens",this.state.itens);

  itens.map((item, i) => {
    bodyFormData.append("valor", item.valor);
    bodyFormData.append(
      "emolumento_residencia_id",
      item.emolumento_residencia_id
    );
    bodyFormData.append(
      "inscricao_residencia_id",
      item.inscricao_residencia_id
    );
    bodyFormData.append("mes", item.mes);
    bodyFormData.append("ano", item.ano);
    bodyFormData.append("forma_pago", item.forma_pago);
    bodyFormData.append("obs", item.obs);
    // bodyFormData.append('total',this.state.total);

    axios
      .post("http://localhost:8000/save_pagamentoResidencia", bodyFormData)
      .then((res) => {
        printDocument();
      });
  });
}
/*getDividas(estudante_id) {
  axios
    .get(`http://192.168.10.150/api/getDividas/${estudante_id}`)
    .then((res) => {
      const dividas = res.data;
      // console.log(curso);
      this.setState({ dividas: dividas });
    });
}*/

const showModal=(id) =>{
  //this.setState({ show: true });
  setShow(true)
  const result = itens.find((item) => item.id == id);
 // this.setState({ itemSel: result });
  setItemSel(result)
 // this.setState({ taxaTemp: result.taxa });
  setTaxaTemp( result.taxa);
}
const hideModal=()=> {
 // this.setState({ show: false });
  setShow(false)
  actualizarTotal(taxaTemp);
}
const showModalDelete=(index) =>{
  //this.setState({ showDelete: true });
  setShowDelete(true)
 // this.setState({ indexDelete: index });
  setIndexDelete(index)
}
const hideModalDelete=()=> {
 // this.setState({ showDelete: false });
  setShowDelete(false)
}

const showModalConfirmFactura=()=> {
 // this.setState({ showConfirmFactura: true });
  setShowConfirmFactura(true)
}
const hideModalConfirmFactura=()=> {
 // this.setState({ showConfirmFactura: false });
  setShowConfirmFactura(false)
}
const confirmFactura=() =>{
  hideModalConfirmFactura();
  savePagamentos();
}

const onChangeTaxa=(e)=> {
  const taxaDig = e.target.value;
  const item = itemSel;
  item.taxa = taxaDig;
  //item.valor=parseInt(item.valor)+parseInt(taxaDig);
  /*this.setState({itemSel:{    
   ...this.state.itemSel,
    taxa:taxaDig   
 }})*/
 // this.setState({ itemSel: item });
  setItemSel(item);
}
const onChangeFormaPago=(e) =>{
  const forma_pago = e.target.value;
  const item = itemSel;
  item.forma_pago = forma_pago;
 // this.setState({ itemSel: item });
  setItemSel(item);
}
const onChangeObs=(e)=> {
  const obs = e.target.value;
  const item = itemSel;
  item.obs = obs;
 // this.setState({ itemSel: item });
  setItemSel(item);

}

const onChangeQtd=(e) =>{
  const qtd = e.target.value;
  const item = itemSel;
  item.qtd = qtd;
  //this.setState({ itemSel: item });
  setItemSel(item);

}

/*onChangeDesconto(e) {
  const desconto = e.target.value;
  const item = this.state.itemSel;
  item.desconto = desconto;
  this.setState({ itemSel: item });
}*/

const onChangePrecoUnit=(e)=> {
  const punit = e.target.value;
  const item = itemSel;
  item.punit = punit;
 // this.setState({ itemSel: item });
  setItemSel(item);

}
const actualizarTotal=(prevTaxa)=> {
  const item = itemSel;
  const taxa = item.taxa;
  if (prevTaxa !== taxa) {
    const valorSemTaxa = parseInt(item.valor) - parseInt(prevTaxa);
    const novaTaxa = parseInt(valorSemTaxa) + parseInt(taxa);
    item.valor = novaTaxa;
   // this.setState({ itemSel: item });
  setItemSel(item);

  }
 // this.setState({ taxaTemp: 0 });
  setTaxaTemp(0);
}

const printDocument=()=> {
  //this.setState({ imprimirFactura: true });
  setImprimirFactura(true)
}
const resetActivarPagamento=()=> {
 // this.setState({ activarPagamento: 0 });
  setActivarPagamento(0)
}

return (
  <div>
    {!imprimirFactura ? (
      <main className="grid">
        <div className="head">
          <h3 className="text-center">Selecione o Estudante</h3>
          <Select_est getEstudante={getEstudante} />
        </div>
        <div id="documentPdf" className="main">
          <Residencia_TableItens
            itens={itens}
            deleteItem={deleteItem}
            showModal={showModal}
            showModalConfirmFactura={showModalConfirmFactura}
            total={total}
            imprimir={printDocument}
          />
        </div>
        {estudanteSel != null ? (
          <Side
            estudanteSel={estudanteSel}
            pagamentoSeleccionado={pagamentoSeleccionado}
            activarPagamento={activarPagamento}
            resetActivarPagamento={resetActivarPagamento}
          />
        ) : (
          ""
        )}
        <div className="footer">
          <div className="row">
            <div className="col-2">
              <div className="row">
                <div className="col-4">
                  <BsDashCircle
                    onClick={decrementar}
                    color="green"
                    size={20}
                  />
                </div>
                <div className="col-4">
                  <span style={{ fontSize: "18px" }}>
                    {contador}
                  </span>
                </div>
                <div className="col-4">
                  <BsPlusCircle
                    onClick={aumentar}
                    color="green"
                    size={20}
                  />
                </div>
              </div>
            </div>
            <div className="col-10">
              <Residencia_Select_emol seleccionado={seleccionado} />
            </div>
          </div>
        </div>
        <Residencia_Modal
          show={show}
          handleClose={hideModal}
          itemSel={itemSel}
          onChangeFormaPago={onChangeFormaPago}
          onChangeObs={onChangeObs}
          onChangeQtd={onChangeQtd}
          onChangePrecoUnit={onChangePrecoUnit}
        />
        <Residecia_ModalDelete
          show={showDelete}
          handleClose={hideModalDelete}
          confirmDelete={confirmDelete}
        />
        <Residencia_ModalConfirmFactura
          show={showConfirmFactura}
          handleClose={hideModalConfirmFactura}
          confirmFactura={confirmFactura}
        />
      </main>
    ) : (
      <Factura
        itens={itens}
        estudante={estudanteSel}
      />
    )}
  </div>
);
}

if (document.getElementById("example")) {
  ReactDOM.render(
    <React.StrictMode>
      <Provider store={store}>
        <Residencia2 />
      </Provider>
    </React.StrictMode>,
    document.getElementById("example")
  );
}