import React, { useEffect, useState } from "react";

const Componente_pagamentoSemFazer = ({
  pagamento,
  pagamentoSeleccionado,
  index,
  activarPagamento,
  resetActivarPagamento
}) => {
  const [pagamentoState, setPagamento] = useState({});
  const [activo, setActivo] = useState(true);

  useEffect(() => {
    setPagamento(pagamento);
  }, [pagamento]);

  useEffect(() => {
    if (activarPagamento == pagamentoState.mes) {
      setActivo(true);
    }
    resetActivarPagamento();
  }, [activarPagamento]);

  const handleClick = () => {
    pagamentoSeleccionado(pagamentoState);
    setActivo(false);
  };

  return (
    <tr>
      <td>
        <button
          type="button"
          className="btn btn-light btn-sm position-relative"
        >
          <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-info">
            {pagamentoState.mes}
          </span>
        </button>
      </td>
      <td className="text-center">
        {" "}
        {activo && (
          <button className="btn btn-primary btn-sm" onClick={handleClick}>
            Pagar
          </button>
        )}
        {!activo && (
          <button disabled className="btn btn-primary btn-sm">
            Pagar
          </button>
        )}
      </td>
    </tr>
  );
};

export default Componente_pagamentoSemFazer;
