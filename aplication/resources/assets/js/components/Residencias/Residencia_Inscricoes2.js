import React, { useEffect, useState } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import ItemResidencia_inscricoes from "./ItemResidencia_inscricoes";
import Inscricao_Modal from "./Inscricao_Modal";



const Residencia_Inscricoes2 = ({
  inscricoes,
  interno,
  setInscricaoSelected,
  getInscricoes,
  eliminarInscricao
}) => {
  // const [inscricoes, setInscricoes] = useState([]);
 // const [quartos, setQuartos] = useState([]);
  const [indexSelected, setIndexSelected] = useState(0);
  const [show, setShow] = useState(false);
  const [quarto, setQuarto] = useState(0);
  const [meses, setMeses] = useState(0);
  const [valor, setValor] = useState(0);
  const [anoAcademico, setAnoAcademico] = useState(0);



 /* const getQuartos = () => {

   axios.get(`http://localhost:8000/api/getQuartos_residencia`).then((res) => {
      const quartos = res.data;
      setQuartos(quartos);
    });
  };*/
  const saveInscricao = () => {
    const bodyFormData = new FormData();
    bodyFormData.append("interno_id", interno.id);
    bodyFormData.append("quarto_id", quarto);
    bodyFormData.append("meses", meses);
    bodyFormData.append("valor", valor);
    bodyFormData.append("anoAcademico", anoAcademico);

    axios
      .post("http://localhost:8000/api/registrarInscricaoInterno", bodyFormData)
      .then((res) => {});
  };

 /* useEffect(() => {
    getQuartos();
  }, []);*/

  const showModal = () => {
    setShow(true);
  };
  const hideModal = () => {
    setShow(false);
    getInscricoes();
  };
  const onChangeQuarto = (e) => {
    const quarto = e.target.value;
    setQuarto(quarto);
  };
  const onChangeMeses = (e) => {
    const meses = e.target.value;
    setMeses(meses);
  };
  const onChangeValor = (e) => {
    const valor = e.target.value;
    setValor(valor);
  };

  const onChangeAnoAcademico = (e) => {
    const anoAcad = e.target.value;
    console.log(e.target.value);
    setAnoAcademico(anoAcad);
  };
  const handleSalvar = () => {
    saveInscricao();
    getInscricoes();
    hideModal();
  };

  if (inscricoes.length != 0) {
    return (
      <div>
        <div className="card card-primary">
          <div className="card-header">
            <p>Inscriões</p>
          </div>
          <div className="card-body">
            <button onClick={showModal} className="btn btn-primary btn-xs">
              Registrar Inscrição
            </button>
            <table className="table table-bordered table-striped">
              <tbody>
                <tr>
                 <th>Ano Academico</th>
                  <th>Quarto</th>
                  <th>Meses</th>
                  <th>Valor</th>
                  <th>Data Entrada</th>
                  <th>Data Saida</th>
                  <th></th>
                </tr>
                {inscricoes.map((inscricao) => (
                  <ItemResidencia_inscricoes
                    key={inscricao.id}
                    inscricao={inscricao}
                    setInscricaoSelected={setInscricaoSelected}
                    indexSelected={indexSelected}
                    eliminarInscricao={eliminarInscricao}
                  />
                ))}
              </tbody>
            </table>
          </div>
        </div>
        <Inscricao_Modal
         key={1}
          show={show}
          handleClose={hideModal}
          handleSalvar={handleSalvar}
          interno={interno}
        //  quartos={quartos}
          onChangeQuarto={onChangeQuarto}
          onChangeMeses={onChangeMeses}
          onChangeValor={onChangeValor}
          onChangeAnoAcademico={onChangeAnoAcademico}
        />
      </div>
      
    );
  } else {
    return (
      <div>
        <div className="card card-primary">
          <div className="card-header">
            <p>Inscrições</p>
          </div>
          <div className="card-body">
            <button onClick={showModal} className="btn btn-primary btn-xs">
              Registrar Inscrição
            </button>
            <div className="text-center">
              <p>Sem Inscrições</p>
            </div>
          </div>
        </div>
        <Inscricao_Modal
        key={2}
          show={show}
          handleClose={hideModal}
          handleSalvar={handleSalvar}
          interno={interno}
        //  quartos={quartos}
          onChangeQuarto={onChangeQuarto}
          onChangeMeses={onChangeMeses}
          onChangeValor={onChangeValor}
          onChangeAnoAcademico={onChangeAnoAcademico}
        />
      </div>
    );
  }
};

export default Residencia_Inscricoes2;
