import React, { useEffect, useState } from "react";
import PagamentosConta from "./Residencia_PagamentosConta";
import Residencia_Inscricoes from "./Residencia_Inscricoes2";
import Residencia_pagamentos from "./Residencia_pagamentos";
import axios from "axios";
import Residencia_pagamentosSemFazer from "./Residencia_pagamentosSemFazer";

const Side = ({ estudanteSel, pagamentoSeleccionado,activarPagamento,resetActivarPagamento }) => {

  const [pagamentosFeitos, setPagamentosFeitos] = useState([]);
  const [inscricoes, setInscricoes] = useState([]);
  const[inscricaoSelected,setInscricaoSelected]=useState(null);

  const getPagamentos = (selection, inscricao) => {
  
    axios
      .get(
        `http://localhost:8000/api/pagamentos_residencia/${selection}/${inscricao}`
      )
      .then((res) => {
        const pagamentos = res.data;
        //pagamentos feitos
        setPagamentosFeitos(pagamentos);
      });
    
  };



  const getInscricoes = () => {
    const interno_id = estudanteSel.id;
    axios
      .get(`http://localhost:8000/api/getInscricoes_residencia/${interno_id}`)
      .then((res) => {
        const inscricoes = res.data;
        setInscricoes(inscricoes);
      });
  };
  const eliminarInscricao = (inscricao_id) => {
   
    axios
      .get(`http://localhost:8000/api/eliminarInscricao_residencia/${inscricao_id}`)
      .then((res) => {
        setInscricaoSelected(null);
        getInscricoes();
      });
  };
  


  useEffect(() => {
   // setIndex(0);
   // setMeses(0);
    getInscricoes();
    setInscricaoSelected(null);
  }, [estudanteSel]); 

  useEffect(() => {
    if(inscricaoSelected!=null){
      //console.log(inscricaoSelected);
       getPagamentos(estudanteSel.id, inscricaoSelected.id);
     

    }
   }, [inscricaoSelected]); 

  return (
    <div className="side">
      <Residencia_Inscricoes
        interno={estudanteSel}
        inscricoes={inscricoes}
        getInscricoes={getInscricoes}
        setInscricaoSelected={setInscricaoSelected}
        eliminarInscricao={eliminarInscricao}
      />
      {inscricaoSelected != null ? (
        <div className="row">
          <div className="col-6">
            <Residencia_pagamentos
              pagamentos={pagamentosFeitos}
              //estudante={estudanteSel}
            />
          </div>
          <div className="col-6">
            <Residencia_pagamentosSemFazer
              pagamentosFeitos={pagamentosFeitos}
              pagamentoSeleccionado={pagamentoSeleccionado}
              activarPagamento={activarPagamento}
              resetActivarPagamento={resetActivarPagamento}
              inscricaoSelected={inscricaoSelected}

            />
          </div>
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

export default Side;
