import React, { useState, useEffect } from "react";
import axios from "axios";
import Comp_internosDisp from "./Comp_internosDisp";
import Comp_internosOcup from "./Comp_internosOcup";

const Componente_quartoT1 = ({
  id,
  codigo,
  capacidade,
  showModal,
  setQuarto
}) => {
  const [ocupados, setOcupados] = useState(0);
  const [internos, setInternos] = useState([]);
  const [fondo, setFondo] = useState("");
  const [full, setFull] = useState(false);
  const [cargando, setCargando] = useState(true);
  const [disponiveis, setDisponiveis] = useState(0);

  const getOcupados = (quarto_id) => {
    axios
      .get(
        `http://localhost:8000/api/getOcupadosQuarto_residencia/${quarto_id}`
      )
      .then((res) => {
        const qtd = res.data;
        setOcupados(qtd);
      });
    setCargando(false);
  };
  const getInternos = (quarto_id) => {
    axios
      .get(
        `http://localhost:8000/api/getInternosQuarto_residencia/${quarto_id}`
      )
      .then((res) => {
        const internos = res.data;
        setInternos(internos);
      });
  };
  const cancelarInscricao_residencia = (interno_id) => {
    axios
      .get(
        `http://localhost:8000/api/cancelarInscricao_residencia/${interno_id}`
      )
      .then((res) => {
        alert("Inscricao Cancelada con sucesso");
        getInternos(id);
      });
  };
  const otraFuncionQualquiera=()=>{
    alert("entro en la otra funcion qualquiera")
  }

  useEffect(() => {
    getOcupados(id);
    getInternos(id);
    calcular();
  }, [id]);

  useEffect(() => {
    if (ocupados < capacidade) {
      setFondo("seagreen");
    }
    if (ocupados == capacidade) {
      setFull(true);
      setFondo("indianred");
    }
  }, [ocupados]);

  const calcular = () => {
    const ocupados = internos.length;
    console.log("ocupados: ", ocupados);
    const disponiveis = capacidade - ocupados;
    console.log("disponiveis: ", ocupados);

    setDisponiveis(disponiveis);
  };

  useEffect(() => {
    calcular();

    return () => {
      setDisponiveis(0);
    };
  }, [internos]);

 

  const registrarInscricao = (e) => {
    e.stopPropagation();
    if (!full) {
      setQuarto(id);
      showModal();
    }
  };
  const cancelarInscricaoTeste = (interno_id) => {
    //e.stopPropagation();
      alert(interno_id);
    cancelarInscricao_residencia(interno_id);
  // otraFuncionQualquiera()
  };

  return (
    <div
      className="row  quarto"
      key={id}
      style={{
        backgroundColor: fondo,
        cursor: "pointer"
        // color: color
      }}
    >
      <h6>Quarto: {codigo}</h6>

      {internos.map((interno) => (
        <div className="col-2"  onClick={()=>{cancelarInscricao_residencia(interno.id)}}>
          <img
           className="rounded-circle alineadoTextoImagenCentro"
            src={"http://localhost:8000/storage/" + interno.pathImage}
          />
        </div>
      ))}

      {[...Array(disponiveis)].map(() => (
        <div className="col-2">
          <img
            onClick={registrarInscricao}
            className="rounded-circle alineadoTextoImagenCentro"
            src="http://localhost:8000/storage/user.png"
          />
        </div>
      ))}
    </div>
  );
};

export default Componente_quartoT1;
