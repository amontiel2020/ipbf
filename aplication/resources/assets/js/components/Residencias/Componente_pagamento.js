import React, { useState } from "react";

const Componente_pagamento = ({ pagamento }) => {
  return (
    <tr>
      <td>
        <button
          type="button"
          className="btn btn-light btn-sm position-relative"
        >
          <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-info">
            {pagamento.mes}
          </span>
        </button>
      </td>
      <td>
        {pagamento.valor}
        <span className="badge rounded-pill bg-success">Pago</span>
      </td>
    </tr>
  );
};

export default Componente_pagamento;
