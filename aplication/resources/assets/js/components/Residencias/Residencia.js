import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import {store} from "../../store";
import axios from "axios";
import Select_est from "./Residencia_Select_est2";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";

import "../estilos.css";
import "bootstrap/dist/css/bootstrap.min.css";

import { BsPlusCircle } from "react-icons/bs";
import { BsDashCircle } from "react-icons/bs";
import Factura from "./Residencia_PDF";
import TableItens from "./Residencia_TableItens";
import PagamentosConta from "./Residencia_PagamentosConta";
import Residencia_Select_emol from "./Residencia_Select_emol";
import Residencia_TableItens from "./Residencia_TableItens";
import Residecia_ModalDelete from "./Residencia_ModalDelete";
import Residencia_Modal from "./Residencia_Modal";
import Residencia_ModalConfirmFactura from "./Residencia_ModalConfirmFactura";
import Residencia_Inscricoes from "./Residencia_Inscricoes";
import Side from "./Side";
import { useInternosQuery } from "../../services/internoApi";


export default class Residencia extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contador: 1,
      contadorItens: 1,
      itens: [],
      itemSel: null,
      pagamentos: [],
      estudanteSel: null,
      emolumento: null,
      show: false,
      showDelete: false,
      taxaTemp: 0,
      indexDelete: null,
      imprimirFactura: false,
      showConfirmFactura: false,
      total: 0,
      dividas: 0,
      valor: 0,
      activarPagamento: 0,
      inscricaoSelected: 0
    };

  const { data:internos, error, isLoading, isSuccess } = useInternosQuery();


    this.decrementar = this.decrementar.bind(this);
    this.aumentar = this.aumentar.bind(this);
    this.seleccionado = this.seleccionado.bind(this);
    this.setItens = this.setItens.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.getEmolumento = this.getEmolumento.bind(this);
    this.getEstudante = this.getEstudante.bind(this);
    this.getPagamentos = this.getPagamentos.bind(this);
    this.pagamentoSeleccionado = this.pagamentoSeleccionado.bind(this);
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.showModalDelete = this.showModalDelete.bind(this);
    this.hideModalDelete = this.hideModalDelete.bind(this);
    this.showModalConfirmFactura = this.showModalConfirmFactura.bind(this);
    this.hideModalConfirmFactura = this.hideModalConfirmFactura.bind(this);
    this.confirmFactura = this.confirmFactura.bind(this);

    // this.onChangeTaxa = this.onChangeTaxa.bind(this);
    this.onChangeQtd = this.onChangeQtd.bind(this);
    // this.onChangeDesconto = this.onChangeDesconto.bind(this);
    this.onChangePrecoUnit = this.onChangePrecoUnit.bind(this);

    this.actualizarTotal = this.actualizarTotal.bind(this);
    this.confirmDelete = this.confirmDelete.bind(this);
    this.printDocument = this.printDocument.bind(this);
    this.savePagamentos = this.savePagamentos.bind(this);
    this.onChangeFormaPago = this.onChangeFormaPago.bind(this);
    this.onChangeObs = this.onChangeObs.bind(this);
    //this.getDividas=this.getDividas.bind(this);
    this.pagamentoDividas = this.pagamentoDividas.bind(this);
    this.getValor = this.getValor.bind(this);
    this.resetActivarPagamento = this.resetActivarPagamento.bind(this);
    this.findInterno = this.findInterno.bind(this);

    
  }

  decrementar() {
    this.setState({
      contador: this.state.contador - 1
    });
  }
  aumentar() {
    this.setState({
      contador: this.state.contador + 1
    });
  }
  seleccionado(id) {
    // alert(id);
    axios.get(`http://192.168.10.150/api/emolumento/${id}`).then((res) => {
      const emolumento = res.data;
      //console.log(emolumento);
      this.setState({ emolumento: emolumento });
      const itensCopy = Array.from(this.state.itens);
      itensCopy.push({
        id: this.state.emolumento.id,
        nome: this.state.emolumento.nome,
        punit: this.state.emolumento.valor,
        qtd: this.state.contador,
        taxa: 0,
        desc: 0,
        valor: this.state.emolumento.valor * this.state.contador,
        ano: 0,
        emolumento_id: this.state.emolumento.id,
        obs: "TPA",
        mes: 0,
        descrip: "",
        divida: 0
      });
      this.setItens(itensCopy);
    });
  }
  pagamentoSeleccionado(pagamento) {
    /*  this.setState({
      contadorItens: this.state.contadorItens + 1
    });
    this.setState({
      inscricaoSelected: index
    });*/

    const itensCopy = Array.from(this.state.itens);
    //this.getValor();
    itensCopy.push({
      //id: this.state.contadorItens,
      id: pagamento.id,
      nome: "Mês " + pagamento.mes,
      ano: pagamento.ano,
      punit: pagamento.valor,
      qtd: 1,
      valor: pagamento.valor,
      emolumento_residencia_id: pagamento.emolumento_residencia_id,
      inscricao_residencia_id: pagamento.inscricao_residencia_id,
      mes: pagamento.mes,
      forma_pago: "TPA",
      obs: ""
    });
    this.setItens(itensCopy);
  }
  pagamentoDividas(valor) {
    const valorPagar = parseInt(valor);

    const itensCopy = Array.from(this.state.itens);
    itensCopy.push({
      id: "Divida",
      nome: "Divida",
      ano: 0,
      punit: valorPagar,
      qtd: 1,
      taxa: valorPagar,
      desc: 0,
      valor: valorPagar,
      emolumento_id: 1,
      mes: 0,
      obs: "TPA",
      descrip: "Multas em atraso",
      divida: 1
    });
    this.setItens(itensCopy);
  }
  setItens(itens) {
    this.setState({ itens: itens });
  }

  deleteItem(index) {
    this.showModalDelete();
    this.setState({ indexDelete: index });
  }

  confirmDelete() {
    var i = this.state.itens.findIndex((x) => x.id == this.state.indexDelete);
    var item = this.state.itens.find((x) => x.id == this.state.indexDelete);
    var mes = item.mes;
    var listaItens = [...this.state.itens];
    listaItens.splice(i, 1);
    this.setItens(listaItens);
    this.setState({ indexDelete: null });
    this.hideModalDelete();
    this.setState({ activarPagamento: mes });
  }

  inserirEmolumento() {
    alert("OK");
  }

  getEmolumento(id) {
    axios
      .get(`http://192.168.10.150/api/emolumentoResidencia/${id}`)
      .then((res) => {
        const emolumento = res.data;
        //console.log(emolumento);
        this.setState({ emolumento: emolumento });
      });
  }
findInterno(id){
  const interno=internos.find((interno)=>interno.id==id);
  return interno;
}

  getEstudante(selection) {
  const interno= this.findInterno(selection);
  this.setState({ estudanteSel: interno });

   /* axios
      .get(`http://localhost:8000/api/getEstudanteResidencia/${selection}`)
      .then((res) => {
        const estudante = res.data;
        //console.log(estudante);
        this.setState({ estudanteSel: estudante });
        //  this.getPagamentos(this.state.estudanteSel.id);

        //this.getDividas(this.state.estudanteSel.id);
      });*/
  }

  getValor() {
    const inscricao_id = this.state.inscricaoSelected;
    axios
      .get(`http://localhost:8000/api/getValorInscricao/${inscricao_id}`)
      .then((res) => {
        console.log(res.data);
        const valor = res.data;
        this.setState({ valor: valor });
      });
  }

  getPagamentos(selection) {
    //console.log("Paramentros",selection,anoAcademico);
    axios
      .get(`http://localhost:8000/api/pagamentos_residencia/${selection}`)
      .then((res) => {
        const pagamentos = res.data;
        this.setState({ pagamentos: pagamentos });
        //console.log(pagamentos);
      });
  }
  savePagamentos() {
    /* const headers = { 
      'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
  };*/
    const bodyFormData = new FormData();

    bodyFormData.append("interno_id", this.state.estudanteSel.id);
    //bodyFormData.append("itens",this.state.itens);

    this.state.itens.map((item, i) => {
      bodyFormData.append("valor", item.valor);
      bodyFormData.append(
        "emolumento_residencia_id",
        item.emolumento_residencia_id
      );
      bodyFormData.append(
        "inscricao_residencia_id",
        item.inscricao_residencia_id
      );
      bodyFormData.append("mes", item.mes);
      bodyFormData.append("ano", item.ano);
      bodyFormData.append("forma_pago", item.forma_pago);
      bodyFormData.append("obs", item.obs);
      // bodyFormData.append('total',this.state.total);

      axios
        .post("http://localhost:8000/save_pagamentoResidencia", bodyFormData)
        .then((res) => {
          this.printDocument();
        });
    });
  }
  /*getDividas(estudante_id) {
    axios
      .get(`http://192.168.10.150/api/getDividas/${estudante_id}`)
      .then((res) => {
        const dividas = res.data;
        // console.log(curso);
        this.setState({ dividas: dividas });
      });
  }*/

  showModal(id) {
    this.setState({ show: true });
    const result = this.state.itens.find((item) => item.id == id);
    this.setState({ itemSel: result });
    this.setState({ taxaTemp: result.taxa });
  }
  hideModal() {
    this.setState({ show: false });
    this.actualizarTotal(this.state.taxaTemp);
  }
  showModalDelete(index) {
    this.setState({ showDelete: true });
    this.setState({ indexDelete: index });
  }
  hideModalDelete() {
    this.setState({ showDelete: false });
  }

  showModalConfirmFactura() {
    this.setState({ showConfirmFactura: true });
  }
  hideModalConfirmFactura() {
    this.setState({ showConfirmFactura: false });
  }
  confirmFactura() {
    this.hideModalConfirmFactura();
    this.savePagamentos();
  }

  onChangeTaxa(e) {
    const taxaDig = e.target.value;
    const item = this.state.itemSel;
    item.taxa = taxaDig;
    //item.valor=parseInt(item.valor)+parseInt(taxaDig);
    /*this.setState({itemSel:{    
     ...this.state.itemSel,
      taxa:taxaDig   
   }})*/
    this.setState({ itemSel: item });
  }
  onChangeFormaPago(e) {
    const forma_pago = e.target.value;
    const item = this.state.itemSel;
    item.forma_pago = forma_pago;
    this.setState({ itemSel: item });
  }
  onChangeObs(e) {
    const obs = e.target.value;
    const item = this.state.itemSel;
    item.obs = obs;
    this.setState({ itemSel: item });
  }

  onChangeQtd(e) {
    const qtd = e.target.value;
    const item = this.state.itemSel;
    item.qtd = qtd;
    this.setState({ itemSel: item });
  }

  /*onChangeDesconto(e) {
    const desconto = e.target.value;
    const item = this.state.itemSel;
    item.desconto = desconto;
    this.setState({ itemSel: item });
  }*/

  onChangePrecoUnit(e) {
    const punit = e.target.value;
    const item = this.state.itemSel;
    item.punit = punit;
    this.setState({ itemSel: item });
  }
  actualizarTotal(prevTaxa) {
    const item = this.state.itemSel;
    const taxa = item.taxa;
    if (prevTaxa !== taxa) {
      const valorSemTaxa = parseInt(item.valor) - parseInt(prevTaxa);
      const novaTaxa = parseInt(valorSemTaxa) + parseInt(taxa);
      item.valor = novaTaxa;
      this.setState({ itemSel: item });
    }
    this.setState({ taxaTemp: 0 });
  }

  printDocument() {
    this.setState({ imprimirFactura: true });
  }
  resetActivarPagamento() {
    this.setState({ activarPagamento: 0 });
  }

  render() {
    return (
      <div>
        {!this.state.imprimirFactura ? (
          <main className="grid">
            <div className="head">
              <h3 className="text-center">Selecione o Estudante</h3>
              <Select_est getEstudante={this.getEstudante} />
            </div>
            <div id="documentPdf" className="main">
              <Residencia_TableItens
                itens={this.state.itens}
                deleteItem={this.deleteItem}
                showModal={this.showModal}
                showModalConfirmFactura={this.showModalConfirmFactura}
                total={this.state.total}
                imprimir={this.printDocument}
              />
            </div>
            {this.state.estudanteSel != null ? (
              <Side
                estudanteSel={this.state.estudanteSel}
                pagamentoSeleccionado={this.pagamentoSeleccionado}
                activarPagamento={this.state.activarPagamento}
                resetActivarPagamento={this.resetActivarPagamento}
              />
            ) : (
              ""
            )}
            <div className="footer">
              <div className="row">
                <div className="col-2">
                  <div className="row">
                    <div className="col-4">
                      <BsDashCircle
                        onClick={this.decrementar}
                        color="green"
                        size={20}
                      />
                    </div>
                    <div className="col-4">
                      <span style={{ fontSize: "18px" }}>
                        {this.state.contador}
                      </span>
                    </div>
                    <div className="col-4">
                      <BsPlusCircle
                        onClick={this.aumentar}
                        color="green"
                        size={20}
                      />
                    </div>
                  </div>
                </div>
                <div className="col-10">
                  <Residencia_Select_emol seleccionado={this.seleccionado} />
                </div>
              </div>
            </div>
            <Residencia_Modal
              show={this.state.show}
              handleClose={this.hideModal}
              itemSel={this.state.itemSel}
              onChangeFormaPago={this.onChangeFormaPago}
              onChangeObs={this.onChangeObs}
              onChangeQtd={this.onChangeQtd}
              onChangePrecoUnit={this.onChangePrecoUnit}
            />
            <Residecia_ModalDelete
              show={this.state.showDelete}
              handleClose={this.hideModalDelete}
              confirmDelete={this.confirmDelete}
            />
            <Residencia_ModalConfirmFactura
              show={this.state.showConfirmFactura}
              handleClose={this.hideModalConfirmFactura}
              confirmFactura={this.confirmFactura}
            />
          </main>
        ) : (
          <Factura
            itens={this.state.itens}
            estudante={this.state.estudanteSel}
          />
        )}
      </div>
    );
  }
}

/*if (document.getElementById("example")) {
  ReactDOM.render(
    <React.StrictMode>
      <Provider store={store}>
        <Residencia />
      </Provider>
    </React.StrictMode>,
    document.getElementById("example")
  );
}*/
