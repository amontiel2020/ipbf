import React, { useEffect, useState } from "react";
import axios from "axios";
import Componente_quartoT1 from "./Componente_quartoT1";

import { useQuartosQuery } from "../../services/quartoApi";

const Componente_base = ({ id, showModal, setQuarto }) => {
  const [quartos, setQuartos] = useState([]);
  const [base, setBase] = useState(null);
  const [cargandoComponente, setCargandoComponente] = useState(false);

  const { data: valores, error, isLoading, isSuccess } = useQuartosQuery();

  /* const getBase = (base_id) => {
    axios
      .get(`http://localhost:8000/api/getBase_residencia/${base_id}`)
      .then((res) => {
        const baseResult = res.data;
        setBase(baseResult);
      });
  };*/

  const filterQuartos = (base_id) => {
    const quartosFiltrados = valores.filter(
      (quarto) => quarto.base_id == base_id
    );
    return quartosFiltrados;
  };

  const getQuartos = (id) => {
    const quartosFiltrados = filterQuartos(id);

    setQuartos(quartosFiltrados);
  };

  useEffect(() => {
    console.log(valores);
    getQuartos(id);

    getBase(id);
  }, [id]);

  return (
    <div className=" card base">
      <h3 className="card-title">Base {id}</h3>
      <div className="card-body">
        {quartos.map((quarto) => (
          <Componente_quartoT1
            id={quarto.id}
            codigo={quarto.codigo}
            capacidade={quarto.capacidad}
            showModal={showModal}
            setQuarto={setQuarto}
          />
        ))}
      </div>
    </div>
  );
};

export default Componente_base;
