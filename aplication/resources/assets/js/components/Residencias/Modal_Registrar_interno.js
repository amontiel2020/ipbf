import React, { useState } from "react";
import "./modalResidencias.css";
import Contentor_bases from "./Contentor_bases";
import axios from "axios";

const Modal_Registrar_interno = ({ handleClose, show, estudantes,getnternos }) => {
  const [estudanteSel, setEstudanteSel] = useState(null);
  const showHideClassName = show ? "modal display-block" : "modal display-none";

  const handleSelectEstudante = (e) => {
    const estudante_id = e.target.value;
    const est = estudantes.find((estudante) => estudante.id == estudante_id);
    console.log(est);
    setEstudanteSel(est);
  };

  const registrarInterno = () => {

    const bodyFormData = new FormData();
    bodyFormData.append("nome", estudanteSel.nome);
    bodyFormData.append("estudante_id", estudanteSel.id);
    bodyFormData.append("pathImage", estudanteSel.pathImage);


    axios.post(`http://localhost:8000/api/registrarInterno`,bodyFormData).then((res) => {
    
    });
  };

  const handleSave = () => {
    registrarInterno();
    handleClose();
    getnternos()
  };

  const fecharModal=()=>{
    setEstudanteSel(null);
    handleClose();
  }
  return (
    <div className={showHideClassName}>
      <section className="modal-main">
        <div className="card bg-light">
          <div className="card-body">
            <div className="form-group">
              <label>Estudantes</label>
              <select
                name="estudantes"
                className="form-control"
                onChange={handleSelectEstudante}
              >
                {estudantes.map((estudante) => (
                  <option key={estudante.id} value={estudante.id}>
                    {estudante.nome}
                  </option>
                ))}
              </select>
              <br />
              <br />
            </div>

            {estudanteSel != null && (
              <div className="card">
                <h3 className="card-title">Ficha Estudante Interno</h3>
                <div className="card-body">
                  <img
                    height="150"
                    width="150"
                    src={
                      "http://localhost:8000/storage/" + estudanteSel.pathImage
                    }
                  />
                  <p>
                    <span style={{ fontSize: 20 }}>
                      Nome: {estudanteSel.nome}
                    </span>
                  </p>
                </div>
              </div>
            )}
          </div>
        </div>
        <button className="btn btn-primary btn-sm" type="button" onClick={handleSave}>
          Salvar
        </button>
        <button
          className="btn btn-primary btn-sm"
          type="button"
          onClick={fecharModal}
        >
          Fechar
        </button>
      </section>
    </div>
  );
};

export default Modal_Registrar_interno;
