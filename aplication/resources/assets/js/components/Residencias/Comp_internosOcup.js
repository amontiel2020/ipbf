import React, { useState, useEffect } from "react";
import axios from "axios";

const Comp_internosOcup = ({ internos }) => {

  return (
    <div className="row">
      {internos.map((interno) => (
        <div className="col-2">
          <img
            className="rounded-circle alineadoTextoImagenCentro"
            src={"http://localhost:8000/storage/" + interno.pathImage}
          />
        </div>
      ))}
    </div>
  );
};

export default Comp_internosOcup;
