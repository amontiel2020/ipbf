import React from "react";
import "../modal.css";

const Modal = ({
  handleClose,
  show,
  children,
  itemSel,
  onChangeFormaPago,
  onChangeObs,
  onChangeQtd,
  onChangePrecoUnit
}) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none";

  return (
    <div className={showHideClassName}>
      <section className="modal-main">
        {itemSel != null && (
          <div className="card bg-light">
            <div className="card-body">
              <div className="row">
                <div className="col-6">
                  <div className="form-group">
                    <label>Nome</label>
                    <input
                      disabled
                      type="text"
                      className="form-control"
                      name="nome"
                      value={itemSel.nome || ""}
                    />
                  </div>
                  <div className="form-group">
                    <label>Preço Unitario</label>
                    <input
                      type="text"
                      className="form-control"
                      name="punit"
                      value={itemSel.punit || ""}
                      onChange={onChangePrecoUnit}
                    />
                  </div>
                  <div className="form-group">
                    <label>Quantidade</label>
                    <input
                      type="text"
                      className="form-control"
                      name="qtd"
                      value={itemSel.qtd || ""}
                      onChange={onChangeQtd}
                    />
                  </div>
                  <div className="form-group">
                    <label>Observação</label>
                    <textarea
                      type="text"
                      className="form-control"
                      name="descrip"
                      value={itemSel.obs || ""}
                      rows="3"
                      onChange={onChangeObs}
                    />
                  </div>
                </div>
                <div className="col-6">
                  <div className="form-group">
                    <label>Valor</label>
                    <input
                      type="text"
                      className="form-control"
                      name="valor"
                      value={itemSel.valor || ""}
                    />
                  </div>
                  <div className="form-group">
                    <label>Forma Pagamento </label>
                    <select
                      className="form-control"
                      onChange={onChangeFormaPago}
                      value={itemSel.forma_pago || ""}
                    >
                      <option value="TPA">TPA</option>
                      <option value="Transferência">Transferência</option>
                      <option value="Dinheiro">Dinheiro</option>
                    </select>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        )}

        <button
          className="btn btn-primary btn-sm"
          type="button"
          onClick={handleClose}
        >
          Fechar
        </button>
      </section>
    </div>
  );
};
export default Modal;
