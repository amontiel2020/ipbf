import React from "react";
import Pdf from "react-to-pdf";
import TableItem from "./Residencia_TableItem";
import Residencia_GetMes from "./Residencia_GetMes";

import "bootstrap/dist/css/bootstrap.min.css";
import "../factura.css";
import moment from "moment";

const ref = React.createRef();

const PDF = ({ props, itens, estudante }) => {
  const total = itens.reduce(
    (total, currentValue) => (total = total + parseInt(currentValue.valor)),
    0
  );
  return (
    <div>
      <Pdf targetRef={ref} filename="pagamentoResidencia.pdf">
        {({ toPdf }) => <button onClick={toPdf}>Gerar PDF</button>}
      </Pdf>
     
      <div className="pdf" ref={ref}>
        <div className="container">
          <div className="row">
            <div className="col-12" align="center">
              <img
                width="50px"
                height="50px"
                src="http://localhost:8000/storage/logo.png"
              />

              <p>
                <b>ESCOLA SUPERIOR POLITÉCNICA DE BENGUELA</b>
              </p>
              <p> Decreto Presidencial nº 168/12 de 24 de Julho</p>
              <p>Recibo de pagamentos</p>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
                <b>Nome do Estudante:</b>&nbsp; &nbsp;{estudante.nome}{" "}
              
              </p>
              <p>
                <b>Curso:</b>
                &nbsp;
            
                &nbsp;&nbsp;<b>Quarto:</b>&nbsp;
               
                &nbsp;&nbsp;<b>Ano Acadêmico:</b>&nbsp;{"2021/2022"}
              </p>

              <p align="right">
                <b>Data:</b>&nbsp;{moment().format("DD-MM-YYYY")}
              </p>
            </div>
          </div>

          <div className="row">
            <table className="table_test">
            <thead>
              <tr>
                <th>Descrição</th>
                <th>P.Unit</th>
                <th>Qtd</th>
                <th>Forma de Pago</th>
                <th>Total</th>
                <th>Obs</th>
              </tr>
              </thead>
              <tbody>
                {itens.map((item, i) => (
                  <tr key={i}>
                    <td className="item">
                   
                      {item.mes}
                      
                    </td>
                    <td className="item">
                      {new Intl.NumberFormat("pt-PT", {
                        minimumFractionDigits: 2
                      }).format(item.punit)}
                    </td>
                    <td className="item">{item.qtd}</td>
                    <td className="item">{item.forma_pago}</td>
                    <td className="item">
                      {new Intl.NumberFormat("pt-PT", {
                        minimumFractionDigits: 2
                      }).format(item.valor)}
                    </td>
                    <td>{item.obs}</td>
                  </tr>
                ))}
                <tr>
                  <td align="right" colSpan="8" style={{ fontSize: "15px" }}>
                    <span>
                      Total:{" "}
                      {new Intl.NumberFormat("pt-PT", {
                        minimumFractionDigits: 2
                      }).format(total)}{" "}
                      {"Kz"}
                    </span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <br />
          <div className="row">
            <div className="col-6">
              <p>
                <b>Assinatura do estudante</b>
                ______________________________________
              </p>
            </div>
            <div className="col-6">
              <p>
                <b>Nome do funcionário</b>
                __________________________________________
              </p>
            </div>
          </div>

          <br />
          <p align="center">
            Telef. +244 996616277/921226215 - Email: espbenguela@gmail.com
            Bairro da Graça - Benguela Angola
          </p>
          <p className="linia">
            ------------------------------------------------------------------------------------------------------
            --------------------------------------------------------------------------------------------------------------------------
          </p>
        </div>

        <div className="container">
          <div className="row">
            <div className="col-12" align="center">
              <img
                width="50px"
                height="50px"
                src="http://localhost:8000/storage/logo.png"
              />

              <p>
                <b>ESCOLA SUPERIOR POLITÉCNICA DE BENGUELA</b>
              </p>
              <p> Decreto Presidencial nº 168/12 de 24 de Julho</p>
              <p>Recibo de pagamentos</p>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
                <b>Nome do Estudante:</b>&nbsp; &nbsp;{estudante.nome}{" "}
                &nbsp;&nbsp;&nbsp;<b>Nº do BI:</b> &nbsp;
   
              </p>
              <p>
                <b>Curso:</b>
                &nbsp;
    
                &nbsp;&nbsp;<b>Quarto:</b> &nbsp;
    
                &nbsp;&nbsp;<b>Ano Acadêmico:</b>&nbsp;{"2021/2022"}
              </p>

              <p align="right">
                <b>Data:</b>&nbsp;{moment().format("DD-MM-YYYY")}
              </p>
            </div>
          </div>

          <div className="row">
            <table className="table_test">
            <thead>
              <tr>
                <th>Descrição</th>
                <th>P.Unit</th>
                <th>Qtd</th>
                <th>Forma de Pago</th>
                <th>Total</th>
                <th>Obs</th>
                
              </tr>
              </thead>
              <tbody>
                {itens.map((item, y) => (
                  <tr key={y}>
                    <td className="item">
                    
                      {item.mes}
                    
                    </td>
                    <td className="item">
                      {new Intl.NumberFormat("pt-PT", {
                        minimumFractionDigits: 2
                      }).format(item.punit)}
                    </td>
                    <td className="item">{item.qtd}</td>
                    <td className="item">{item.forma_pago}</td>
                    <td className="item">
                      {new Intl.NumberFormat("pt-PT", {
                        minimumFractionDigits: 2
                      }).format(item.valor)}
                    </td>
                    <td>{item.obs}</td>
                  </tr>
                ))}
                <tr>
                  <td align="right" colSpan="8" style={{ fontSize: "15px" }}>
                    <span>
                      Total:{" "}
                      {new Intl.NumberFormat("pt-PT", {
                        minimumFractionDigits: 2
                      }).format(total)}{" "}
                      {"Kz"}
                    </span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <br />
          <div className="row">
            <div className="col-6">
              <p>
                <b>Assinatura do estudante</b>
                ______________________________________
              </p>
            </div>
            <div className="col-6">
              <p>
                <b>Nome do funcionário</b>
                __________________________________________
              </p>
            </div>
          </div>

          <br />
          <p align="center">
            Telef. +244 996616277/921226215 - Email: espbenguela@gmail.com
            Bairro da Graça - Benguela Angola
          </p>
        </div>
      </div>
    </div>
  );
};

export default PDF;
