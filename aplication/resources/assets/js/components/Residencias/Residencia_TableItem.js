import React, { Component } from "react";
import ReactDOM from "react-dom";


import "../estilos.css";
import { BsXCircle } from "react-icons/bs";


//const todos={todos};
export default class Residencia_TableItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nome: " ",
      punit: 0,
      qtd: 0,
      valor: 0
    };

    this.deleteItem = this.deleteItem.bind(this);
    this.handleOnMouseClick = this.handleOnMouseClick.bind(this);
    this.showModal = this.showModal.bind(this);
    this.getNome = this.getNome.bind(this);
  }

  deleteItem(e) {
    e.stopPropagation();
    const { deleteItem } = this.props;
    //deleteItem(e.target.id);
    deleteItem(e.currentTarget.getAttribute("data-id"));
    console.log(
      "index en table item ",
      e.currentTarget.getAttribute("data-id")
    );
  }
  handleOnMouseClick(e) {
    alert("OK");
  }

  getNome(nome) {
    // const {nome}=this.props;
    //console.log("Nome:", nome );
    switch (nome) {
      case "1":
        return "Mês 1";
        break;
      case "2":
        return "Mês 2";
        break;
      case "3":
        return "Mês 3";
        break;
      case "4":
        return "Mês 4";
        break;
      case "5":
        return "Mês 5";
        break;
      case "6":
        return "Mês 6";
        break;
      case "7":
        return "Mês 7";
        break;
      case "8":
        return "Mês 8";
        break;
      case "9":
        return "Mês 9 ";
        break;
      case "10":
        return "Mês 10";
        break;

      default:
        return nome;
    }
  }
  showModal(e) {
    const { showModal } = this.props;
    let id = e.currentTarget.getAttribute("data-id");
    showModal(id);
  }
  /*componentDidUpdate(newProps) {
    if (newProps != this.props) {
      this.setState({
        taxa: newProps.newTaxa,
        desconto: newProps.newDesconto
      });
    }
  }*/

  render() {
    return (
      <tr
        onClick={this.showModal}
        key={this.props.id}
        data-id={this.props.id}
        onMouseOver={this.changeBackground}
        onMouseLeave={this.changeBackground2}
        style={{ cursor: "pointer" }}
        className="linhaItem"
      >
        <td width="2%" className="botonDeleteItem">
          <BsXCircle
            className="BsXCircleDelete"
            onClick={this.deleteItem}
            data-id={this.props.id}
            color="red"
            size={15}
          />
        </td>
        <td className="item">
          {<span>{this.getNome(this.props.nome)}</span> }
        
        </td>
        <td className="item">
          {new Intl.NumberFormat("pt-PT", {
            minimumFractionDigits: 2
          }).format(this.props.punit)}
        </td>
        <td className="item">
          {this.props.qtd}
        </td>
        <td className="item">
          {this.props.forma_pago}
        </td>
        <td className="item">
          {new Intl.NumberFormat("pt-PT", {
            minimumFractionDigits: 2
          }).format(this.props.valor)}
        </td>
      </tr>
    );
  }
}
