import React, { Component, useState } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import ModalDividas from "./ModalDividas";

//const [id,task,completed]=useState({todo});
export default class RegistrarDivida extends Component {
  constructor(props) {
    super(props);
    this.state = {valorAumentar:0,show: false };

    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.handleSaveDividas = this.handleSaveDividas.bind(this);
    this.onChangeValor = this.onChangeValor.bind(this);
    this.setDividas = this.setDividas.bind(this);

   
  }

  setDividas(estudante_id, valor) {
    axios
      .get(`http://192.168.10.150/api/setDividas/${estudante_id}/${valor}`)
      .then((res) => {
        const dividas = res.data;
        this.setState({ dividas: dividas });
      });
  }

  showModal() {
    this.setState({ show: true });
  }
  hideModal() {
    this.setState({ show: false });
  }

  onChangeValor(e) {
    const valor = e.target.value;
    this.setState({ valorAumentar: valor });

  }

  handleSaveDividas() {
  
    const id = this.props.id;
    const valor = this.state.valorAumentar;
    this.setDividas(id, valor);
    this.hideModal();
  }

  render() {
    return (
      <div>
        {" "}
        <button className="btn btn-primary btn-sm" onClick={this.showModal}>
          Inserir Divida
        </button>
        {" "}
        {this.state.valorAumentar > 0 ? (<span><b>{this.state.valorAumentar}</b></span> ) : ("")}
        <ModalDividas
          show={this.state.show}
          handleClose={this.hideModal}
          onChangeValor={this.onChangeValor}
          handleSaveDividas={this.handleSaveDividas}
        />
      </div>
    );
  }
}
