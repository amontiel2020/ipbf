import React, { Component, useState } from "react";
import ReactDOM from "react-dom";

import ItemRecurso from "./ItemRecurso";
import Export from "./Export";

import { BsCardList } from "react-icons/bs";

//const todos={todos};
export default class TableRecursos extends Component {
  constructor(props) {
    super(props);


    this.state = { recursos: [], total: 0 };

  }





  componentDidUpdate(newProps) {
    //console.log("props itens",newProps.itens);
    //console.log("newProps itens",newProps.itens);
    if (newProps != this.props) {
      //  this.setState({itens:newProps.itens})
      // this.setTotal();
    }
  }



  render() {
    return (
      <div>
        <div>
          {this.props.recursos != null && (
            <table className="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nº</th>
                  <th>Unidade Curricular</th>

                </tr>
              </thead>
              <tbody>
                {this.props.recursos.map((item, i) => (
                  <ItemRecurso
                    key={i}
                    id={item.id}
                    nome={item.nome}
                  />
                ))}
              </tbody>
            </table>
          )}

          <div></div>
        </div>
      </div>
    );
  }
}
