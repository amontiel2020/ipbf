import React from "react";
import "./modal.css";

const ModalDividas = ({
  handleClose,
  show,
  children,
  onChangeValor,
  handleSaveDividas
 
}) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none";

  return (
    <div className={showHideClassName}>
      <section className="modal-main">
      
          <div className="card bg-light">
            <div className="card-body">
              <div className="row">
                <div className="col-6">
                  <div className="form-group">
                    <label>Valor</label>
                    <input
                      type="text"
                      className="form-control"
                      name="valor"
                      
                     onChange={onChangeValor}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
       

        <button
          className="btn btn-primary btn-sm"
          type="button"
          onClick={handleSaveDividas}
        >
          Registrar
        </button>


        <button
          className="btn btn-primary btn-sm"
          type="button"
          onClick={handleClose}
        >
          Fechar
        </button>
      </section>
    </div>
  );
};
export default ModalDividas;
