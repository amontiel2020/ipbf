import React, { Component, useState } from "react";
import ReactDOM from "react-dom";

import ItemPagamentoRecurso from "./ItemPagamentoRecurso";
import Export from "./Export";

import { BsCardList } from "react-icons/bs";

//const todos={todos};
export default class TablePagamentosRecursos extends Component {
  constructor(props) {
    super(props);


    this.state = { pagamentos: [], total: 0 };

  }





  componentDidUpdate(newProps) {
    //console.log("props itens",newProps.itens);
    //console.log("newProps itens",newProps.itens);
    if (newProps != this.props) {
      //  this.setState({itens:newProps.itens})
      // this.setTotal();
    }
  }



  render() {
    return (
      <div>
        <div>
          {this.props.pagamentosRecurso != null && (
            <table className="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nº</th>
                  <th>Data</th>
                  <th>Obs</th>
                  

                </tr>
              </thead>
              <tbody>
                {this.props.pagamentosRecurso.map((item, i) => (
                  <ItemPagamentoRecurso
                    key={i}
                   
                    nome={item.data}
                    descrip={item.descrip}
                  />
                ))}
              </tbody>
            </table>
          )}

          <div></div>
        </div>
      </div>
    );
  }
}
