import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TablePagamentosRecursos from './TablePagamentosRecursos';
import TableRecursos from './TableRecursos';

export default class Informacao extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-4 col-md-offset-2">
                        <div className="panel panel-default">
                            <div className="panel-heading">Recursos </div>

                            <div className="panel-body">
                              <TableRecursos recursos={this.props.recursos}/>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 col-md-offset-2">
                        <div className="panel panel-default">
                            <div className="panel-heading">pagamentos</div>

                            <div className="panel-body">
                              <TablePagamentosRecursos pagamentosRecurso={this.props.pagamentosRecursos}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


