import React, { Component } from "react";
import ReactDOM from "react-dom";
import Option_estudante from "./Option_estudante";
import Modal from "./Modal.js";
import "./estilos.css";
import { BsXCircle } from "react-icons/bs";
import GetMes from "./GetMes";
import Mostrar_curso from "./Mostrar_curso";

//const todos={todos};
export default class ItemRecurso extends Component {
  constructor(props) {
    super(props);


  }


  render() {
    return (
      <tr 
        key={this.props.id} data-id={this.props.id} 
        style={{ cursor: "pointer" }}
      >
        <td>{this.props.key}</td>
        <td>{this.props.nome}</td>
 

      </tr>
    );
  }
}
