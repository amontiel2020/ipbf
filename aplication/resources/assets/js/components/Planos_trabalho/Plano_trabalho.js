import React, { Component } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import Add_actividade from "./Add_actividade";

export default class Plano_trabalho extends Component {
  constructor(props) {
    super(props);
    this.state = { actividades:[] };

    this.getActividades = this.getActividades.bind(this);
   
  }

  getActividades() {
   const funcionario_id=10;
    axios
      .get(`http://localhost:8000/api/getActividades/${funcionario_id}`)
      .then((res) => {
        const actividades = res.data;

        this.setState({ actividades: actividades });
      });
  }

  componentWillMount() {
    this.getActividades();
  }

  render() {
    if (this.state.actividades.length != 0) {
      return (
        <div>
        <h1>Plano de Trabalho do funcionario ""</h1>
        <Add_actividade
          getActividades={this.getActividades}
        />
        <table className="table table-bordered table-striped">
        <tbody>
          <tr>
            <th>Actividades</th>
            <th>Resultaos Esperados</th>
            <th>Indicador</th>
            <th>Area</th>
            <th>Responsavel</th>
            <th>Periodo</th>
          </tr>
        {this.state.actividades.map((act) => (
          <tr key={act.id}>
          <td>{act.nome}</td>
          <td>{act.meta}</td>
          <td>{act.indicador}</td>
          <td>{act.departamento_id}</td>
          <td>{act.funcionario_id}</td>
          <td>{act.periodo}</td>
          </tr>
        ))}
        </tbody>
      </table>
      </div>
      );
    } else {
      return  <Add_actividade
      getActividades={this.getActividades}
    />;
    }
  }
}
if (document.getElementById("plano_trabalho")) {
  ReactDOM.render(
    <Plano_trabalho />,
    document.getElementById("plano_trabalho")
  );
}