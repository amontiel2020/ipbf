import React from "react";
import "../modal.css";

const Modal_addActividade = ({
  handleClose,
  show,
  children,
  onChangeNome,
  onChangeMeta,
  onChangeIndicador,
  onChangeArea,
  onChangeResponsavel,
  onChangePeriodo,
  handleSaveActividade,
  funcionarios
}) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none";

  return (
    <div className={showHideClassName}>
      <section className="modal-main">
        <div className="card bg-light">
          <div className="card-body">
            <div className="row">
              <div className="col-6">
                <div className="form-group">
                  <label>Nome</label>
                  <textarea
                    type="text"
                    className="form-control"
                    name="nome"
                    onChange={onChangeNome}
                    defaultValue={""}
                  />
                </div>

                <div className="form-group">
                  <label>Metas</label>
                  <textarea
                    type="text"
                    className="form-control"
                    name="meta"
                    onChange={onChangeMeta}
                    defaultValue={""}

                  />
                </div>

                <div className="form-group">
                  <label>Indicador</label>
                  <textarea
                    type="text"
                    className="form-control"
                    name="indicador"
                    onChange={onChangeIndicador}
                    defaultValue={""}

                  />
                </div>
              </div>
              <div className="col-6">
              <div className="form-group">
                  <label>Responsavel</label>
                  <select  onChange={onChangeResponsavel} name="responsavel" className="form-control">
                <option defaultValue={0}>-</option>
                  {funcionarios.map((funcionario) => (
                    <option key={funcionario.id} value={funcionario.id}>{funcionario.nome_completo}</option>
                  ))}
                  </select>
                </div>
                <div className="form-group">
                  <label>Area</label>
                  <select  onChange={onChangeArea} name="area" className="form-control">
                  <option defaultValue={0}>-</option>
                    <option value="1">Area1</option>
                    <option value ="3">Area2</option>
                  </select>
                </div>
                <div className="form-group">
                  <label>Periodo</label>
                  <select  onChange={onChangePeriodo} name="periodo" className="form-control">
                  <option defaultValue={0}>-</option>
                    <option value="I">I</option>
                    <option value="II">II</option>
                    <option value="III">III</option>
                    <option value="IV">IV</option>

                  </select>
                </div>

              </div>
            </div>
          </div>
        </div>

        <button
          className="btn btn-primary btn-sm"
          type="button"
          onClick={handleSaveActividade}
        >
          Registrar
        </button>

        <button
          className="btn btn-primary btn-sm"
          type="button"
          onClick={handleClose}
        >
          Fechar
        </button>
      </section>
    </div>
  );
};
export default Modal_addActividade;
