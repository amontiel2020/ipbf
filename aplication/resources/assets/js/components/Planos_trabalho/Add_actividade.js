import React, { Component, useState } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import Modal_addActividade from "./Modal_addActividade";

//const [id,task,completed]=useState({todo});
export default class Add_actividade extends Component {
  constructor(props) {
    super(props);
    this.state = {show: false,nome:"",meta:"",indicador:"",responsavel:0,area:0,periodo:"" ,funcionarios:[]};

    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.onChangeNome = this.onChangeNome.bind(this);
    this.onChangeMeta = this.onChangeMeta.bind(this);
    this.onChangeIndicador = this.onChangeIndicador.bind(this);
    this.onChangeArea = this.onChangeArea.bind(this);
    this.onChangeResponsavel = this.onChangeResponsavel.bind(this);
    this.onChangePeriodo = this.onChangePeriodo.bind(this);
    this.handleSaveActividade = this.handleSaveActividade.bind(this);
    this.saveActividade = this.saveActividade.bind(this);
    this.reset = this.reset.bind(this);

  
  

   
  }
  getActividades(){
    const { getActividades } = this.props;
    getActividades();

  }


  saveActividade() {
    const bodyFormData = new FormData();
   
    const nome = this.state.nome;
    const meta = this.state.meta;
    const indicador = this.state.indicador;
    const periodo = this.state.periodo;
    const responsavel = this.state.responsavel;
    const area = this.state.area;

    bodyFormData.append('nome',nome);
    bodyFormData.append('meta',meta);
    bodyFormData.append('indicador',indicador);
    bodyFormData.append('responsavel',responsavel);
    bodyFormData.append('area',area);
    bodyFormData.append('periodo',periodo);


      axios
      .post(`http://localhost:8000/saveActividade`,bodyFormData)
      .then((res) => {
        this.getActividades();
        this.reset()
      });
  }
  reset(){
    this.setState({nome:""});
    this.setState({meta:""});
    this.setState({indicador:""});
    this.setState({responsavel:0});
    this.setState({area:0});
    this.setState({periodo:""});



  }

  getFuncionarios() {
    //console.log("Paramentros",selection,anoAcademico);
    axios
      .get(`http://localhost:8000/api/getFuncionarios`)
      .then((res) => {
        const funcionarios = res.data;
        this.setState({ funcionarios: funcionarios });
       });
  }
  componentWillMount() {
    this.getFuncionarios();
  }

  showModal() {
   
    this.setState({ show: true });
  }
  hideModal() {
    this.setState({ show: false });
  }

  onChangeNome(e) {
    const nome = e.target.value;
    this.setState({ nome: nome });

  }
  onChangeMeta(e) {
    const meta = e.target.value;
    this.setState({ meta: meta });

  }

  onChangeIndicador(e) {
    const indicador = e.target.value;
    this.setState({ indicador: indicador });

  }
  onChangeResponsavel(e) {
    const responsavel = parseInt(e.target.value);
   // alert(responsavel);
    this.setState({ responsavel: responsavel });
    console.log(this.state.responsavel);

  }
  onChangeArea(e) {
    const area = parseInt(e.target.value);
    this.setState({ area: area });

  }
  onChangePeriodo(e) {
    const periodo = e.target.value;
    this.setState({ periodo: periodo });

  }

  handleSaveActividade() {
  
    this.saveActividade();
    this.hideModal();
  }

  render() {
    return (
      <div>
        {" "}
        <button className="btn btn-primary btn-sm" onClick={this.showModal}>
          Registrar Actividade
        </button>
        {" "}
   
        <Modal_addActividade
          show={this.state.show}
          handleClose={this.hideModal}
          onChangeNome={this.onChangeNome}
          onChangeMeta={this.onChangeMeta}
          onChangeIndicador={this.onChangeIndicador}
          onChangeArea={this.onChangeArea}
          onChangeResponsavel={this.onChangeResponsavel}
          onChangePeriodo={this.onChangePeriodo}
          handleSaveActividade={this.handleSaveActividade}
          funcionarios={this.state.funcionarios}
        />
      </div>
    );
  }
}
