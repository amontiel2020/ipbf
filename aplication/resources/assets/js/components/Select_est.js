import React, { Component, useState } from "react";
import Select from "react-select";
import axios from "axios";

export default class Select_est extends Component {
  constructor(props) {
    super(props);
    this.state = { estudantes: [], options: [], id: "", nome: "" };

    this.getEstudante = this.getEstudante.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  // get all estudantes from backend
  getEstudantes() {
    axios.get(`http://192.168.10.150/api/estudantes_react`).then((res) => {
      const estudantes = res.data;
      // console.log(estudantes);

     /* label: <div><img src={
        "http://192.168.10.150/storage/" +
        estudante.pathImage
      } height="30px" width="30px"/>{estudante.nome} </div>*/

      const options = estudantes.map((estudante) => ({
        value: estudante.id,
        label: estudante.nome
      }));

      this.setState({ estudantes: estudantes });
      this.setState({ options: options });
    });
  }
  getEstudante(e) {
    const { getEstudante } = this.props;
    getEstudante(e.target.value);
  }
  componentWillMount() {
    this.getEstudantes();
  }
  handleChange(e) {
    this.setState({ id: e.value, name: e.label });
    const { getEstudante } = this.props;
    getEstudante(e.value);
  }

  render() {
    let optionItems = this.state.estudantes.map((estudantes) => (
      <option key={estudantes.id}>{estudantes.nome}</option>
    ));
    return (
      <div className="card">
        <div className="card-body">
         {/* <select className="form-control" onChange={this.getEstudante}>
            {this.state.estudantes.map((estudante) => (
              <option value={estudante.id} key={estudante.id}>
                {estudante.nome}
              </option>
            ))}
          </select>*/}
         <Select options={this.state.options} onChange={this.handleChange} />
        </div>
      </div>
    );
  }
}
