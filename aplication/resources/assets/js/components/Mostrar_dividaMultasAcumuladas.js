import React, { Component, useState } from "react";
import ReactDOM from "react-dom";
import axios from "axios";

import RegistrarDivida from "./RegistrarDivida";

//const [id,task,completed]=useState({todo});
export default class Mostrar_dividaMultasAcumuladas extends Component {
  constructor(props) {
    super(props);
    this.state = { dividas: 0, valorPagar: 0};

    this.getDividas = this.getDividas.bind(this);
    this.onChangeValorPagar = this.onChangeValorPagar.bind(this);
    this.pagamentoDividas = this.pagamentoDividas.bind(this);
  
  
  

    
  }
  getDividas(estudante_id) {
    axios
      .get(`http://192.168.10.150/api/getDividas/${estudante_id}`)
      .then((res) => {
        const dividas = res.data;
        this.setState({ dividas: dividas });
      });
  }


  pagamentoDividas(e) {
    const { pagamentoDividas } = this.props;
    const valor = this.state.valorPagar;
    pagamentoDividas(valor);
  }
  onChangeValorPagar(e) {
    //console.log(e.target.value);
    if (e.target.value == 0) {
      this.setState({ dividas: this.props.dividas });
    }
    if (parseInt(e.target.value) > 0) {
      const valor = parseInt(e.target.value);

      this.setState({ valorPagar: valor });
      const dividas = this.props.dividas;
      this.setState({ dividas: dividas - valor });
    }
  }
 

  componentDidMount() {
    this.getDividas(this.props.id);
  }

  /* componentWillReceiveProps(newProps) {
    //console.log(this.props.curso_id);
    this.getDividas(newProps.id);
  }*/



  render() {
    return (
      <div className="row">
        {this.props.dividas > 0 ? (
          <div className="col-12 mt-2">
            <div className="card card-pagamentos ms-3">
              <div className="card-header">
                <div className="row">
                  <div className="col-6">
                    <h5>Dividas</h5>
                  </div>
                  <div className="col-6">
                    {" "}
                    <RegistrarDivida
                     id={this.props.id}
                     
                    />
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-6 mt-2">
                    <h2>
                      <span className="label label-danger textoRojo">
                        {new Intl.NumberFormat("pt-PT", {
                          minimumFractionDigits: 2
                        }).format(this.state.dividas)}{" "}
                        {"Kz"}
                      </span>
                    </h2>
                  </div>
                  <div className="col-6 mt-2">
                    <div className="row">
                      <div className="col-6">
                        <div className="form-group">
                          <input
                            className="form-control"
                            type="text"
                            name="pagamentoDivida"
                            defaultChecked="0"
                            onChange={this.onChangeValorPagar}
                          />
                        </div>
                      </div>
                      <div className="col-6 mr-0">
                        <button
                          className="btn btn-primary btn-sm"
                          onClick={this.pagamentoDividas}
                        >
                          Pagar
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <RegistrarDivida
            id={this.props.id}
          />
          
        )
        
        }
      
      </div>
    );
  }
}
