import {
    createApi,
    fetchBaseQuery
  } from "@reduxjs/toolkit/query/react";
  
  export const quartoApi = createApi({
    reducerPath: "quartoApi",
    baseQuery: fetchBaseQuery({
      baseUrl: "http://localhost:8000/api/"
    }),
  
    endpoints: (builder) => ({
      quartos: builder.query({
        query: () => "/quartos"
  
      }),
      
    })
  });
  
 
  export const {
    useQuartosQuery
   } = quartoApi;