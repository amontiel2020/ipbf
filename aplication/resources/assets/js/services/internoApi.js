import {
    createApi,
    fetchBaseQuery
  } from "@reduxjs/toolkit/query/react";
  
  export const internoApi = createApi({
    reducerPath: "internoApi",
    baseQuery: fetchBaseQuery({
      baseUrl: "http://localhost:8000/api/"
    }),
  
    endpoints: (builder) => ({
      internos: builder.query({
        query: () => "/internos"
  
      }),
      
    })
  });
  
  // src/services/taskApi.js
  export const {
    useInternosQuery,
   } = internoApi;